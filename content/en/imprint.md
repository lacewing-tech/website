---
draft: false
title: Imprint
headline: Imprint
subtitle: ""
url: imprint
menu: footer
linkTitle: Imprint
weight: 2
description: LaceWing Techs imprint.
toc: false
images: null
date: 2020-06-19T13:10:43.924Z
---
LaceWing Tech has closed. Read our full statement [here](/blog/closing/).

Lüdtke und Wadden - LaceWing Tech GbR\
Shareholders: Carma Lüdtke & Laura Pierson Wadden

c/o Lüdtke\
Waldemarstr. 54\
10997 Berlin

VAT ID: DE322298300

E-Mail for general inquiries: info @ lacewing.tech\
E-Mail for our customers: support @ lacewing.tech

Phone: +49 157 3673 1901

We use the [Hugo-Theme Terrassa](https://themes.gohugo.io/hugo-terrassa-theme/).

Our [Privacy Policy](/privacy-policy/)