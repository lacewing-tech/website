---
title: "Closing"
headline: "Closing"
subtitle: ""
description: ""
date: 2020-12-22T12:41:06+01:00
publishDate: 2020-11-18T14:41:06+01:00
author: "LaceWing Tech Team"
images:
draft: false
---
2020 has been a hard year for all of us and for LaceWing Tech, it’s been a challenging year to start a new business. In the face of these challenges, we have done our best to find a sustainable way forward, but have decided in the end that it is the best decision to close, effective at the end of this year. This decision was extremely difficult, especially since we love our work, love our clients and love the team.

We will continue to support all clients until the end of their contracts. For those who can't migrate to a new provider as quickly, we will offer the option to continue our service for a finite amount of time. To make the transition to another provider as smooth as possible, we are talking with potential IT support partners and cloud partners.

We have notified all of our clients individually and will provide further information via e-mail in the coming days. If you have any questions about your contract or the next steps, do not hesitate to contact us. We will do the best we can to support you in any way we can.
We are thankful to the people who have supported us these last 2 years - especially all of the staff who put countless hours into LaceWing Tech. We wish you all the best on your next projects!

Although LaceWing Tech is closing, we as individuals will continue building tech that aims to be secure, usable and avoids the big tech monopolies. We still believe in a world where that’s possible and will keep fighting for it!
Thank you again, stay healthy, and happy new year.

All the best,

Laura and Carma
