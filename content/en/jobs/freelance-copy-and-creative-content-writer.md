---
title: "Freelance copy and creative content writer"
headline: "Freelance copy and creative content writer"
linkTitle: ""
subtitle: ""
description: ""
images:
draft: true
menu:
weight: 0
url:
---
LaceWing Tech is an IT company located in Berlin Kreuzberg. We offer secure and sustainable cloud and IT services with a special focus on security and usability.

## We offer
- secure cloud storage (Nextcloud) incl. backups,
- collaborative office tools,
- support and workshops on how to use the cloud,
- hardware and consulting
for small- to medium-sized companies, activists and freelancers.

## We care about
- sustainability,
- being grounded and honest,
- respect and fairness towards our customers and colleagues,
- inclusion,
- a flexible and future-proof company.

## We are looking for:

**a freelance copy- and creative content writer (English and German language)**

at the earliest possible date.

You‘ll be writing engaging English and German copy for our website, advertising campaigns and informational materials in close collaboration with the marketing and sales team. You‘ll take the perspective of our target groups and write conversion-oriented texts that appeal to them. For our customers, you write easy to understand tutorials on how to use our products.

### Requirements:

- Strong English and German writing skills.
- Relevant experience in writing content for web and advertising. We don’t need you to show X years of full-time experience, but you should be able to communicate a message to a customer, empathize with their needs and questions, and be able to show examples of your work.
- Good research skills and the ability to grasp new information quickly.
- You’re interested in technology, privacy, and open source. You have technical background knowledge about cloud technology or at a minimum the ability to grasp new technical concepts and products quickly. Good research skills are a must.
- SEO knowledge is advantageous.

Apart from these skills, we expect you to be competent in Microsoft Office applications, a good communicator and reliable in delivering your work by a deadline.

Please send your application – resume, relevant references and examples of your work – to jobs (at) lacewing.tech. The PDF file should not be larger than 5 MB.

If you have any questions, please contact Tina: tina (at) lacewing.tech.

LaceWing Tech is committed to diversifying our team. We especially welcome applications from people of colour, older people, trans* and non-binary people, and members of groups who often encounter systemic barriers to employment. If there are ways in which you identify that you would like us to take into consideration, please let us know on your application.

We care about privacy. Please review how we handle your data below.

Information for applicants during the application process:

- We process your data in order to fill open job vacancies and to manage the application process.
- Legal basis for data processing is § 26 BDSG.
- This data is necessary for conducting the application process. If you don't provide this data, we cannot process your application.
- All personal data regarding the application process will be deleted six months after the application process is terminated (which means six months after you received a rejection from us), except when we are legally justified or required to process your data further.
