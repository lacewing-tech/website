FROM ubuntu:latest as HUGOINSTALL

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update && \
    apt-get install -y hugo \
                       npm

COPY . /hugo-site

RUN cd /hugo-site &&\
    git submodule init &&\
    git submodule update &&\
    npm install &&\
    hugo -v
