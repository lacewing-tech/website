---
draft: false
headline: Terms & Conditions
subtitle: ""
url: conditions
linkTitle: Terms & Conditions
title: Terms & Conditions
date: 2020-06-22T08:57:14.394Z
images:
weight: 0
description: Our terms and conditions.
menu: footer
---
## General Terms and Conditions (7/2020)

[Conditions (7/2020) as PDF download](/072020-AGB-LüdtkeundWadden-LaceWingTechGbR.pdf)
