---
draft: false
title: Produkte
headline: Produkte
subtitle: Cloud und IT-Service aus einer Hand
url: produkte
menu: main
linkTitle: Produkte
weight: 1
description: "Cloud und IT-Services aus einer Hand: die IT-Komplettlösung zum
  sicheren und effizienten Arbeiten im Team"
toc: false
images: null
date: 2020-06-19T14:19:15.569Z
---
{{% emphasized %}}Die IT-Komplettlösung von LaceWing Tech bietet kleinen bis mittelständischen Firmen und Organisationen alles, was sie brauchen, um effizient zusammenzuarbeiten. Wir kümmern uns um die Technik, damit Teams sich auf ihre Arbeit konzentrieren können.{{% /emphasized %}}

## LaceWing Cloud

Wir nutzen die Open Source Cloud-Software [Nextcloud](/de/produkte/nextcloud/), der seit 2018 auch die deutsche Bundesregierung vertraut. Mit unserer LaceWing Cloud können Teams intern und extern Dateien teilen und mit sorgfältig ausgewählten Office-Apps effizient zusammenarbeiten.

* DSGVO-konform
* Serverseitige Verschlüsselung
* Automatisierte Backups
* DIN ISO/IEC 27001-zertifizierte Server in Deutschland, betrieben mit Ökostrom
* Umfassende Möglichkeiten, Berechtigungen für Nutzer:innen zu verwalten
* OnlyOffice zum gemeinsamen Bearbeiten von Dokumenten
* Kalender, Umfragen, Kanban-Boards u.v.m.

Für eine reibungslose Umstellung zur LaceWing Cloud bieten wir einen **Migrationssupport** an: Wir richten die LaceWing Cloud auf allen Arbeitsplätzen ein und migrieren bestehende Daten.

## LaceWing Consult

Mit LaceWing Consult stellen wir sicher, dass die Technik reibungslos funktioniert und die Bedürfnisse unserer Kund:innen erfüllt. Folgende Leistungen können gebucht werden:

* **IT-Support**: Unsere Techniker*innen stehen per E-Mail und Telefon bei jeglichen Fragen und Problemen zur Verfügung und kümmern sich um eine zeitnahe Lösung.
* **Workshops**: Software ist nur dann sicher, wenn Nutzer:innen das nötige Wissen haben, sie anzuwenden. Deshalb bieten wir Workshops zum sicheren Arbeiten mit der Cloud und IT-Sicherheit an.
* **Consult**: Individuelle Beratung zu Themen wie Kubernetes, Netzwerkinfrastruktur, IT-Infrastrukturprüfung, IT-Umstrukturierung u.v.m. Unser Team kann auf jahrzehntelange Erfahrung in diesen Bereichen bauen, um eine individuelle Lösung zu erarbeiten.

### Workshop-Themen:

* **OnlyOffice**: Kollaboratives Arbeiten an Dokumenten
* **Nextcloud**: Einführung in die Grundfunktionen
* **Passwortverwaltung**: Passwörter sicher teilen
* **Sicherheit und Komfort im Internet**: Risiken verstehen und vermeiden
* **Home Office**: effizient und sicher von zu Hause arbeiten und dabei die Zufriedenheit des Teams steigern.

## LaceWing Hardware

Wir vermieten und verkaufen vorkonfigurierte Hardware, die bereits die notwendige Software enthält und sofort einsatzfähig ist. Dazu gehören Server, Laptops, Mini-PCs, Tablets und Smartphones. Aus Gründen der Nachhaltigkeit beziehen wir noch fast ungenutzte Ausstellungsstücke von hoher Qualität, die wir mit sicherer Software aufrüsten.

{{% button href="/de/kontakt/" %}}Lass uns sprechen!{{% /button %}}