---
draft: true
title: IT-Administration und IT-Support
headline: IT-Administration und IT-Support
subtitle: ""
url: jobs/it-administration-und-it-support
weight: 0
description: Wir suchen zwei feste Mitarbeiter:innen für IT-Administration und
  IT-Support in Voll- oder Teilzeit
images:
date: 2020-12-04T15:56:43.324Z
linkTitle: ""
menu: null
---
LaceWing Tech ist ein IT-Unternehmen mit Sitz in Berlin Kreuzberg, das sichere und benutzerfreundliche Cloud- und IT-Dienstleistungen anbietet.

## Wir bieten KMUs, Aktivist:innen und Selbstständigen

* sichere Cloud-Speicherung (Nextcloud) mit Backups,
* kollaborative Office-Tools,
* Support und Workshops zum Arbeiten mit der Cloud,
* Hardware und Consulting.

## Wir legen Wert auf

* Nachhaltigkeit,
* Bodenständigkeit und Ehrlichkeit,
* Respekt und Fairness gegenüber unseren Kund:innen und Mitarbeiter:innen,
* Inklusion,
* und ein flexibles, zukunftssicheres Unternehmen.

**\- Bewerbungsfrist verlängert bis 10. Dezember 2020 -**

## Wir suchen zum 01. Januar 2021:

**zwei feste Mitarbeiter:innen für IT-Administration und IT-Support in Voll- oder Teilzeit (20 - 40 Stunden pro Monat)**

Deine Hauptaufgabe ist es, sicherzustellen, dass unsere Kund:innen und Mitarbeiter:innen die digitalen Tools haben, die sie brauchen, und sie sicher und mit so wenig Kopfschmerzen wie möglich anwenden können. Als Teil unseres IT-Services- und Internal-IT-Teams ist deine Rolle, unsere IT-Systeme weiterzuentwickeln und zu warten, Support und Trainings intern für unser Team und extern für unsere Kund:innen durchzuführen, und dabei exzellente Dokumentationen zu schreiben. Diese Position ist essentiell bei LaceWing Tech, weil wir glauben, dass Technologie nur dann sicher ist, wenn Nutzer:innen in der Lage sind, sie richtig anzuwenden.

### Zu deinen täglichen Aufgaben gehören:

* das Management unseres Helpdesks (intern und extern) - Support-Tickets analysieren, beantworten, priorisieren und Ideen zu entwickeln, wie unsere Arbeitsprozesse verbessert werden können.
* Co-Konzeption und Durchführung von Workshops für Kund:innen zu unseren Produkten und IT-Sicherheit (Nextcloud, Passwortmanager, Grundlagen von IT-Sicherheit usw.).
* Installation und Wartung von Soft- und Hardware intern und für unsere Kund:innen.
* Mitarbeit an der Weiterentwicklung und der Wartung unserer interner IT-Systeme und -Prozesse mit einem besonderen Fokus auf Qualität, Sicherheit, und kontinuierliche Optimierung.
* Du gehst auf Fehlersuche und findest die Ursache für IT-Probleme und Vorfälle, inklusive Dokumentation und Anleitungen für häufig auftretende Probleme.

### Wir erwarten:

* Exzellente Kommunikationsfähigkeiten auf Deutsch und Englisch (schriftlich und mündlich), insbesondere mit Kund:innen.
* Relevante Erfahrung in IT-Administration und/oder IT-Support und/oder IT-Trainings. Du musst keine X Jahre an Vollzeit-Berufserfahrung vorweisen, aber du solltest in der Lage sein, weitestgehend selbstständig die Ursache von Vorfällen und Problemen ausfindig zu machen, zu improvisieren, mehrere IT-Themen schnell zu recherchieren und zu troubleshooten, und Probleme zu lösen.
* Tiefergehendes Wissen zu entweder MacOS oder Windows; Erfahrungen mit Linux, Nextcloud, und E-Mail-Clients ist von Vorteil.

### Außerdem bist du:

* gut organisierst - du priorisierst mehrere Projekte und Aufgaben, dokumentierst und kommunizierst den aktuellen Stand, und hältst Deadlines ein.
* mit Begeisterung dabei, wenn es darum geht, Wissen zu teilen und durch Pairing, Präsentationen und Dokumentation zu lernen.
* gut darin, Vorfälle und Probleme klar und verständlich zu kommunizieren und deine Erklärungen an das technische Vorwissen deines Publikums anzupassen.
* geduldig und verständnisvoll bei Fehlern (inklusive deiner eigenen!); du siehst Fehler als eine Möglichkeit, zu lernen.
* Du hast Freude daran, neue Dinge und Ideen auszuprobieren.
* Du verstehst, dass eine Vielzahl von Faktoren die Fähigkeit von Menschen beeinflussen, Zugang zu Technologie zu erhalten und sie anzuwenden - deine Herangehensweise berücksichtigt diesen Kontext, ohne herablassend zu werden oder zu versuchen, andere mit Technologie zu “retten”.
* Du unterstützt Open Source und hast ein Interesse daran, nicht-proprietäre Lösungen zu finden.

Weiterhin wünschen wir uns von dir die Fähigkeit, Probleme zu lösen, und die Befähigung zur Teamarbeit. Wir arbeiten momentan aufgrund von Corona von zu Hause und treffen uns ab und zu persönlich für Teambesprechungen. Je nachdem, wie sich die Situation weiter entwickelt, wollen wir perspektivisch zum Teil vom Büro arbeiten und, wenn es notwendig ist, unseren Kund:innen persönliche Trainings, Installationen und Support vor Ort anbieten.

Deine Bewerbung mit aussagekräftigen Bewerbungsunterlagen (kurzes Anschreiben und Lebenslauf) schick uns bitte bis 10. Dezember an [jobs @ lacewing.tech](mailto:jobs@lacewing.tech). Achte bitte darauf, dass die PDF-Datei nicht größer als 5 MB ist.

Fragen zur Stelle beantwortet Dir Carma unter [carma @ lacewing.tech](mailto:carma@lacewing.tech).

Wir begrüßen insbesondere Bewerbungen von BIPOC, älteren Menschen, trans*- und nicht-binären Menschen, und Menschen, die oft mit systematischen Barrieren beim Zugang zum Arbeitsmarkt konfrontiert sind. Wenn es diesbezüglich etwas gibt, von dem du möchtest, dass wir es bei deiner Bewerbung berücksichtigen, teile uns dies bitte in deinen Bewerbungsunterlagen mit.

Bitte beachte unsere Hinweise zum Umgang mit Deinen Daten.

Informationen für Bewerber*innen im Bewerbungsverfahren:

* Wir verwenden die von Dir erhobenen Daten zum Zweck des Besetzungsverfahrens ausgeschriebener freier Arbeitsplätze.
* Rechtsgrundlage für die Datenverarbeitung ist § 26 BDSG.
* Die Bereitstellung der Daten ist für die Durchführung des Bewerbungsverfahrens notwendig. Bei Nichtbereitstellung kann die Bewerbung nicht bearbeitet werden.
* Sämtliche im Rahmen des Bewerbungsverfahrens erhobenen personenbezogenen Daten werden sechs Monate nach Beendigung des Bewerbungsverfahrens (d.h. nach Bekanntgabe der Absageentscheidung) gelöscht, es sei denn, wir sind rechtlich zur weiteren Verarbeitung Deiner Daten berechtigt oder verpflichtet.
