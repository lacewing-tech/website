# LWT Website

Static website for [LaceWing Tech](https://lacewing.tech).

## Stack:
- [Hugo](https://hugo.io) - Static site generator
- [Netlify CMS](https://netlifycms.org) - Headless CMS
- [Nginx docker container](https://hub.docker.com/_/nginx)

## Deployment:
- [Gitlab CI/CD](https://docs.gitlab.com/ee/ci/)
- [Gitlab Container Registry](https://gitlab.com/lacewing-tech/website/container_registry)
- [Kubernetes Container Orchestration](https://kubernetes.io)

## Requirements:
- Hugo (https://gohugo.io/getting-started/installing/)
- Node.js and npm (https://nodejs.org/en/)

## Set up local environment:

Clone repository

In the project folder, run:
```bash
$ cd website
$ git submodule init
$ git submodule update
```

to register submodules and update them.

Then install packages:

```bash
$ npm install
```

## Build

White in development, use Hugo built-in LiveReload to test site:

```bash
$ hugo server
```

For deployment, run:

```bash
$ hugo
```

This will build site in /public directory.

See https://gohugo.io/getting-started/usage/ for details.

## Create new content

### Create new pages

```bash
$ hugo new example.md -k page
```

### Create new blog posts

```bash
$ hugo new posts/example.md
```

## CMS usage

We use [Netlify CMS](https://www.netlifycms.org) as our CMS. It is simply a
javascript file and therefore runs client-side.

To login to the CMS you need a [Gitlab](https://gitlab.com) account and need
write access to this repository.

Login: https://lacewing.tech/admin

Follow 'Sign-in with Gitlab' and grant permission to the Netlify App. Once you
grant this permission, you should be able to draft and publish content on the site.
All of the changes are done through a Git workflow - whenever you `Save` something,
it creates a Merge Request on this repo. When you `Publish`, then it merges it
into master. The automatic pipeline then deploys the change to the site in production.

I haven't yet found good documentation on using the CMS editor. This is something
that might be a net negative for later... Let's keep an eye out for tutorials/guides
for content managers.

## Netlify CMS Configuration

Configuration for Netlify CMS is located in `static/admin/config.yml`.

The `backend` section is where we configure Gitlab as the Netlify CMS backend.
This means that Netlify uses the Gitlab API to create MRs with new content and
also uses it for authorization of users. See the
[NetlifyCMS backend documentation](https://www.netlifycms.org/docs/gitlab-backend/).

The `app_id` is important because it tells NetlifyCMS which Application is
allowed to use Gitlab.com for OAuth. In order to create the `app_id`, follow
[Gitlab's documentation](https://docs.gitlab.com/ee/integration/oauth_provider.html#adding-an-application-through-the-profile) for
creating the Application OAuth access token. NOTE: Do not check
'Confidential', as that prevents access from single-page applications like
Netlify. Also note that *ANY* person can create the token for
`https://lacewing.tech/admin/` and there can be multiple tokens at the
same time. The `app_id` itself can be committed to the repository and it is
can be securely used and stored on the client. It is simply for app
identification purposes, not for any kind of authorization.

## Environments and deployment

The deployment pipeline has two stages - first we [use Kaniko](https://docs.gitlab.com/ee/ci/docker/using_kaniko.html)
to build the docker image (nginx & our static files) and push it to the gitlab
container registry (see 'Packages --> Container Registry' in the left sidebar of
Gitlab).

In the `deploy` stage, we deploy to a Kubernetes cluster hosted on Hetzner,
configured [here](https://gitlab.com/lacewing-tech/infrastructure).

The Kubernetes cluster is connected to Gitlab in 'Operations --> Kubernetes'.
See [this entry for how to setup the cluster properly](https://gitlab.com/lacewing-tech/infrastructure/-/tree/master/kubernetes-manifests#gitlab)
and [this guide about connecting a cluster to gitlab](https://www.digitalocean.com/community/questions/how-to-connect-to-kubernetes-from-gitlab).

We use the Gitlab container registry to save our images and we delete stale
images with the new [Image retention policy](https://gitlab.com/help/user/packages/container_registry/index#expiration-policy).
To change, go to Settings --> CI/CD Settings --> Container Registry --> tag
expiration policy and enable the setting and set to expire after 90 days. Later
we can consider finer controls using regex, etc.

### Branches and MRs

* **tech-feature:** For any technical change create a feature branch (use a name of your preference) out of devel branch. Make sure devel is up-to-date on your local repo before you create this branch.


```bash
$ git checkout devel
$ git checkout -b tech-feature
```

Merging it back to devel will trigger the review stage of pipeline. Make sure you change the target branch to devel when creating the merge request (default would be master if you don't change it).

This branch can be deleted as soon as it's merged to devel.

* **devel:** The up-to-date technical branch. This includes the latest technical changes, as all of the technical branches are merged to it. It doesn't include latest content changes.

You can use this to test technical changes before you merge them to master. Any changes that have reached this branch can be displayed in https://staging.lacewing.de .

Commits on devel trigger the staging stage of pipeline.

You can bundle up multiple technical changes, coming from several tech-feature branches to devel, before you merge devel to master.

Don't delete this branch after a merge to master!

* **cms/...:** The cms branches are automatically created by Netlify cms and include only content changes.

Netlify CMS automatically opens an MR from a cms branch to master.

As soon as an editor publishes a change via Netlify CMS the MR is automatically merged to master and changes are deployed in production stage.

* **master**: The up-to-date technical and content branch. Any change that has reached this branch, will be displayed on the production website.

Merge manually to master only technical changes from devel branch. Content changes are merged automatically by Netlify CMS.

Commits on master trigger the production stage of pipeline.

#### Workflow

A regular workflow for deploying technical changes would be the following:

- Create a tech-feature branch from devel. Once you're done with the changes open an MR to devel and take a look at the review environment. If you want to add more changes, make commits to the tech-feature branch and look into the new pipeline - new review environment.

- Merge the tech-feature branch to devel. This could come along with other changes to devel coming from other technical branches. All of your changes are displayed on https://staging.lacewing.de . QA the changes on it.

- Changes are QA'd and ready to be deployed to production. Open an MR from devel to master. Make sure to disable the option "Delete source branch". Merge this MR and your changes are now on https://lacewing.tech

### Review environment

Each technical merge request from feature branch to devel is automatically deployed to https://branch-name-slug.review.lacewing.de.
This process is managed by something called Gitlab Environments, which gives us
some convenient tools to manage all the review apps.

To view a review app, go to the homepage of your Merge Request. It should show
that a review app is deployed and provide a link to the app.

Note: each review app has basic auth. The credentials can be found in the
Team Keepass (it is the same credentials as for the Relaunch basic auth).

When the MR is merged, the review app will be automatically deleted. If you
would like to stop the review app before an MR is merged, you can navigate to the
"Stop Review" pipeline step and manually stop the review app. This will delete
all associated resources from the server and also 'Stop' the environment in
Gitlab.

See the [Gitlab documentation on Environments](https://docs.gitlab.com/ee/ci/environments/index.html#configuring-dynamic-environments)
to learn more about configuring and using them.

### Staging environment

Devel branch is deployed automatically at https://staging.lacewing.de.
There one can test the latest changes and QA.

Once you're happy with the changes, you can manually deploy the changes to
Production.

### Production environment

We manually deploy the master branch to https://lacewing.tech.
