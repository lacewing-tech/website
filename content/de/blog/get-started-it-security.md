---
title: "4 Tipps, um mit dem Thema IT-Sicherheit zu starten"
headline: "4 Tipps, um mit dem Thema IT-Sicherheit zu starten"
subtitle: ""
description: "Dein Unternehmen steht beim Thema IT-Sicherheit ganz am Anfang? Wir zeigen dir die wichtigsten Bereiche eines ganzheitlichen Sicherheitskonzepts."
date: 2020-11-18T14:41:06+01:00
publishDate: 2020-11-13T14:41:06+01:00
author: "LaceWing Tech Team"
images:
- /images/uploads/get-started-with-it-security.jpg
draft: false
tags: []
url: vier-tipps-um-mit-it-sicherheit-zu-starten
---
Wer kennt das nicht? Du findest ein sensibles Dokument in eurem Teamordner, auf das alle Teammitglieder Zugriff haben, und fragst dich: Ob das wohl wirklich so in Ordnung ist? Oder dein Team speichert eure Passwörter in einem Word-Dokument und du denkst: Ist das wirklich sicher?

In diesem Artikel zeigen wir dir, wie du mit der IT-Sicherheit in deinem Unternehmen startest, wenn ihr noch kein Sicherheitskonzept habt. Wir gehen dabei auf die vier wichtigsten Bereiche ein, die du bei deinem Konzept beachten solltest.

{{< figure src="/images/uploads/get-started-with-it-security.jpg" title="Bild: Patrick Perkins // Unsplash" alt="" width="780">}}

## Wie beginne ich mit dem Thema IT-Sicherheit?
Bevor wir uns ausführlicher mit dem Umsetzen von konkreten Tipps beschäftigen, stellt sich dir bestimmt die Frage: Wie fange ich eigentlich an? Ganz egal, wie weit ihr beim Thema IT-Sicherheit seid: Der erste Schritt sollte immer eine Erfassung des Ist-Zustands sein. Nur so kannst du anschließend festlegen, in welchen Bereichen ihr euch verbessern solltet. Um den Ist-Zustand zu erfassen, können dir folgende Fragen helfen:

- Welche Daten gibt es in eurem Unternehmen?
- Welche dieser Daten sind sensibel?
- Wie werden eure Dateien aktuell geteilt und gespeichert?
- Und von wem?

Es ist dabei wichtig zu verstehen, dass IT-Sicherheit nur dann funktionieren kann, wenn dein Sicherheitskonzept ganzheitlich angelegt ist. Wenn du zum Beispiel nur eure technische Infrastruktur verbesserst und du dir dabei keine Gedanken machst, ob alle wissen, wie sie damit umgehen sollen, dann kann dein Konzept nicht wirken.

Für ein ganzheitliches Konzept solltest du vor allem an folgende vier Punkte denken:

### 1. Bilde deine Mitarbeiter:innen zu Sicherheitsthemen weiter
Das Training deiner Mitarbeiter:innen und eine offene Fehlerkultur sind wichtige Bausteine für ein ganzheitliches IT-Sicherheitskonzept. Es kann durchaus immer mal passieren, dass Fehler beim Thema IT-Sicherheit vorkommen. An diesem Punkt ist es entscheidend, dass ihr offen mit diesen Fehlern umgeht. Alle Teammitglieder sollten ohne Angst ihre Unsicherheiten und Fehler äußern können. Sie sollten Ihre Fehler offen ansprechen können, ohne dass sie dafür mit negativen Auswirkungen rechnen müssen.

Sicherheit ist Teamarbeit! Es ist die Aufgabe des gesamten Teams, Strukturen zu schaffen, die es jedem Einzelnen ermöglichen, die Richtlinien zu befolgen und zu verstehen. Eine wichtige Maßnahme, um das zu erreichen, sind regelmäßige Mitarbeiter:innen-Trainings. In diesen Trainings könnt ihr zum Beispiel Fragen beantworten, wie man sichere Passwörter erstellt oder wie man ein öffentliches WLAN-Netz am besten nutzt.

Neben den internen Fortbildungen könnt ihr außerdem verschiedene weitere Maßnahmen nutzen, um euer Team für das Thema IT-Sicherheit zu sensibilisieren:

- Eine Einführung zur IT-Sicherheit als fester Bestandteil des Onboardings für neue Mitarbeiter:innen.
- Ein Newsletter mit regelmäßigen Sicherheitstipps.
- Eine IT-Sicherheitsrichtlinie, um die wichtigsten Regeln schriftlich für alle einsehbar festzuhalten.
- Externe Workshops zu ausgewählten Thema als zusätzliche Weiterbildung (übrigens: LaceWing Tech bietet genau auf dein Team abgestimmte Workshops an. [Wir beraten dich gerne.](/de/kontakt/)).

### 2. Entscheide dich von Vornherein für sichere, nutzer:innenfreundliche Software
Stelle dir vor, dass eine Mitarbeiter:in gerade unter Zeitdruck steht und eine wichtige Deadline einhalten muss. Er oder sie versucht, die Daten in eure Cloud hochzuladen, um sie mit einer Kolleg:in zu teilen, aber irgendwie funktioniert es nicht so richtig. Die sensiblen Daten auf einen USB-Stick zu ziehen und dann zu teilen, ist hingegen in wenigen Minuten erledigt und die Deadline kann eingehalten werden. Wofür würdest du dich in dieser Situation entscheiden?

Eine sichere Cloud ist nur die halbe Miete - sie muss auch nutzer:innenfreundlich sein. Wenn ihr eine sichere Cloud habt, die Benutzung jedoch sehr kompliziert ist, werden eure Mitarbeiter:innen je nach Situation auf einfachere Alternativen umsteigen.

Außerdem sollte das Thema Sicherheit als Standard in der Software fest verankert sein und nicht nur eine weitere Funktion, die du bei Bedarf nutzen kannst. Wenn eure Cloud zum Beispiel standardmäßig verschlüsselt ist, sind eure Daten sicher. Ihr müsst euch keine weiteren Gedanken darüber machen, ob ihr zusätzliche Sicherheitseinstellungen beachten müsst.

Eine Möglichkeit, wie du eine sichere und dennoch nutzer:innenfreundliche Cloud aufsetzen kannst, ist die Nutzung von Nextcloud über unsere Lacewing Cloud. Wie du es von anderen Setups gewohnt bist, kannst du Daten speichern, synchronisieren oder teilen. Der Unterschied: Deine Daten werden sicher mittels Server Side Encryption verschlüsselt.
### 3. Prüfe, wer Zugang zu deinen Büroräumen hat
Hast du dir schon mal darüber Gedanken gemacht, wie sicher eigentlich eure Büroräume sind? Gerade in der Mittagspause sind viele Büros leer und unbeaufsichtigt. Wäre es bei euch denkbar, dass eine externe Person zu dieser Zeit ins Gebäude kommt und sich eure sensiblen Dokumente auf den Schreibtischen anschaut oder gar Zugang zu einem ungesicherten Rechner hat?

Die Sicherheit deiner Büroräume ist ein weiterer wichtiger Baustein für dein Sicherheitskonzept. Hier solltest du dir vor allem über folgende Fragen Gedanken machen:

- Schließt ihr eure Räume ab, wenn alle Mitarbeiter:innen zum Beispiel in der Mittagspause sind?
- Wer hat Zutritt zu den Büros?
- Sind sensible Dokumente in einem Aktenschrank abgeschlossen?
- Werden sensible Daten häufig offen auf dem Tisch liegen gelassen?

Ein weitverbreiteter Irrtum ist, dass man unbedingt einen Server direkt im eigenen Büro braucht (on-premise Cloud), um eine besonders hohe Datensicherheit zu gewährleisten. Das ist jedoch häufig nicht wirklich sicherer. Denn in eurem Büro kann eingebrochen werden. In den meisten Fällen ist euer Serverraum deutlich schlechter gesichert als eine gehostete Cloud. [Wir beraten euch gerne](/de/kontakt/), welche Lösung (on-premise-, gehostete oder hybrid) für euch sinnvoll ist.

### 4. Führe regelmäßige Backups durch
Nicht immer hat das Thema IT-Sicherheit etwas mit Angriffen durch Externe zu tun. Vermutlich hast du selbst schon einmal versehentlich ein Dokument gelöscht oder du konntest wegen eines technischen Defekts nicht mehr auf deine Daten zugreifen. Damit bist du nicht alleine.

Der Verlust von Daten in Unternehmen ist ein verbreitetes Problem, dass jährlich zu großen finanziellen Schäden führt. In einer Umfrage von Dell Technologies geben 82 Prozent der befragten Unternehmen an, dass sie im vergangenen Jahr Probleme mit Ausfallzeiten oder Datenverlust hatten. Sie gehen davon aus, dass diese Probleme in Zukunft noch weiter zunehmen werden.

Umso wichtiger ist es, dass du deine Backups sicher und regelmäßig durchführst und dadurch Probleme mit Datenverlust vermeidest. Zum einen solltest du einen automatisierten Prozess nutzen. So kannst du nicht vergessen, deine Backups zu erstellen.

Übrigens: Mit der [Lacewing Cloud](/de/produkte/) hast du die Extra-Sicherheit, dass regelmäßige und automatische Backups deiner Daten erstellt werden. Du kannst dir sogar verschiedene Zwischenstände von Dokumenten anschauen und bei Bedarf wiederherstellen.

Wir hoffen, dass wir dir mit diesen Tipps einen guten Einstieg ins Thema Datensicherheit geben konnten. Wenn du mehr zum Thema IT-Sicherheit für kleine und mittelständische Unternehmen erfahren möchtest, [nimm gerne Kontakt zu uns auf.](/de/kontakt/) Wir beraten dich gerne.
