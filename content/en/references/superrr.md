---
title : "SUPERRR - BUILDING DIVERSE & EQUAL FUTURES IN TECH AND BEYOND"
description: ""
logo: "/images/uploads/superrr.svg"
externalLink: "https://superrr.net/"
draft: false
weight: 2
---

&bdquo;We at SUPERRR develop visions and projects for just and equitable futures from a feminist perspective. It was clear that we were looking for a reliable, fair and competent hosting service with a heart for open source technologies - and that's exactly what we found with LaceWing Tech!&rdquo;
