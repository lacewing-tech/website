---
draft: false
headline: Products
subtitle: "Cloud and IT service: all in one"
url: products
linkTitle: Products
title: Products
date: 2020-06-22T14:19:27.252Z
images:
weight: 1
description: "We offer you and your team a complete cloud solution: access to
  our cloud, preconfigured hardware and various IT services."
menu: main
---
{{% emphasized %}}LaceWing Tech’s complete IT solution offers small to medium-sized companies and  organizations everything they need to work together efficiently. We take care of the technology so that your team can focus on your work.{{% /emphasized %}}

## LaceWing Cloud
We use the open source cloud software Nextcloud, which the German government trusts since 2018. With our LaceWing Cloud, teams can share files internally and externally and collaborate efficiently with carefully selected office apps.
- GDPR compliant
- Server-side encryption
- Automated backups
- DIN ISO/IEC 27001 certified servers in Germany, operated with green energy
- Extensive possibilities to manage user permissions
- OnlyOffice for shared document editing
- Calendar, surveys, kanban boards and much more

For a smooth transition to the LaceWing Cloud, we offer **migration support**: we set up the LaceWing Cloud on all workstations and migrate existing data for you.

## LaceWing Consult
With LaceWing Consult, we ensure that our products work smoothly and meet your demands. The following services can be booked:
- **IT Support**: our IT-admins are available via e-mail and phone to answer any questions and solve any problems in a timely manner.
- **Workshops**: software is only safe if users have the necessary knowledge to use it. Our workshops give you and your team the confidence to use your tools securely and efficiently.
- **Consult**: individual consulting on topics such as Kubernetes, network infrastructure, IT infrastructure audits, IT restructuring and much more. Our team has decades of experience in these areas, which it uses to develop bespoke solutions for you.

### Workshop topics:
- **OnlyOffice**: collaborative working on documents
- **Nextcloud**: introduction to Nextcloud’s basic functions
- **Password management**: share passwords securely
- **Digital Security**: understanding and avoiding risks
- **Home Office**: work efficiently and securely from home and increase team satisfaction.

## LaceWing Hardware
We rent and sell pre-configured hardware that already contains the necessary software and is ready to be used. This includes servers, laptops, mini PCs, tablets and smartphones. For environmental reasons, we source almost unused, high-quality hardware, which we upgrade with secure software.

{{% button href="/contact/" %}}Let's talk!{{% /button %}}
