---
headline: Goodbye, Zoom! Open source video conference tools
subtitle: How we manage online meetings at LaceWing Tech
url: blog/open-source-video-conference-tools
title: Goodbye, Zoom! Open source video conference tools
description: What are good open source options for video conferences? Our guide
  helps you choose.
author: Laura Wadden
date: 2020-06-18T10:57:46.545Z
---
We at LaceWing Tech have the same problem as everyone else - how do we meet online in a sustainable, user-friendly, yet secure way? We spent a few months testing various tools and eventually settled on **[Jitsi-Meet](https://jitsi.org/jitsi-meet/)**. It’s open source, easy to use and has all the features one needs for everyday work and meetings.

2 min read

### The options

There aren’t \*that\* many open source video conferencing alternatives and only two are good: Jitsi-Meet and [BigBlueButton](https://bigbluebutton.org/) (see diagram, credits: Swecha).

![Comparison of various video chat alternatives: BigBlueButton, Jitsi, Microsoft teams, Cisco webex, Zoom and Google Meet. Photo by Swecha.](/images/uploads/comparison-various-video-conferencing-tools-swecha.jpg "Video conference tool comparison. Photo by Swecha.")

Both are open source and usable, but good at different things. BigBlueButton is best for presentations and events because it is more powerful, more stable, and has more shiny features — but it takes a lot more resources. Jitsi-Meet is perfect for day-to-day work because it is simple, has just what we need, and uses less resources.

### Other products we tested:

* [Wire](https://wire.com/en/): we use the free Wire version for our team chat and 1-1 video calls. We tested Wire’s ‘Teams’ product for multiple-person video chat and it did not offer enough additional features to be worth the cost.
* [Nextcloud Talk](https://nextcloud.com/talk/): we do not recommend using Nextcloud Talk (yet!) because it is painfully unusable. We will continue to test as the tool improves.

### **Jitsi-Meet in a 10-person team** — **it just works**

For LaceWing Tech’s daily grind, Jitsi-Meet has all we need: screensharing, mute/unmute audio and video, chat, “raise hand”, tile view (as opposed to speaker view), and more.

I have about 4-5 calls per day, mostly on Jitsi, and rarely have problems. I might experience the inevitable “Can you see me? Can you hear me?” dance for 2-3 minutes, but afterwards, things go relatively smoothly. Calls with more than 8 people are also no problem. The setup is easy and the interface is approachable - I can send someone a simple link and they can join from the browser (or on their phone).

Look at these happy people:

![Four people wave and smile in a video call.](/images/uploads/winner-2.png "LaceWing Tech in Jitsi-Meet video chat")

However, when someone has internet problems, things are still tough. Our professional IT tip? Swear at the computer and pray. Other than that, for important meetings I try to have a backup plan ready so that if we’re unlucky, everyone knows what the next steps are.

Jitsi does a great job of keeping audio consistent while video might lag - so if video freezes, you can still have a smooth experience of the meeting. Sometimes we turn our video off to help improve quality.

Jitsi-Meet supports 10 simultaneous users easily, 20 are possible, and 50 are reported possible, but probably require a rather fine-tuned server setup. Though Jitsi-Meet is not yet end-to-end encrypted for more than two participants, working prototypes are already deployed and tested.

Usability/User experience depends on a combination of reliability of internet connection, client, and server setup. Not all Jitsi servers (e.g., <https://meet.systemli.org>) are created equal, so thank you to [Systemli](https://www.systemli.org), a Berlin tech collective and friends of ours, for providing such a stable experience! Kudos also to the Jitsi-Meet team for building and maintaining a user-friendly alternative to 2-billion dollar Zoom. It’s not easy to do, especially when larger companies can spend unlimited amounts of money on User Experience (UX) Designers.

### **What’s Next? Jitsi-Meet at LaceWing Tech**

In the future, LaceWing Tech will offer Jitsi-Meet as a product. [Subscribe to our newsletter](#newsletter) to get the latest updates.