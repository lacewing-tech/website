#!/bin/bash

docker build . -t website-new --no-cache
docker cp $(docker create --rm website-new:latest):/hugo-site/public .
rsync -avz --delete public/ deploy@starling:/var/www/lacewing.tech/html/
