---
title: "Nextcloud Conf 2020: Our highlights and learnings"
headline: "Nextcloud Conf 2020: Our highlights"
subtitle: ""
description: "Did you miss Nextcloud Conf 2020? ► Check out our CTO Laura Wadden's highlights and learnings, with links to recordings."
date: 2020-10-08T15:48:31+02:00
publishDate: 2020-10-08T15:48:31+02:00
author: "Laura Wadden"
images:
- /images/uploads/Nextcloud-Conf-2020.png
draft: false
---
This weekend I attended and spoke at {{< target-blank "Nextcloud Conf 2020" "https://nextcloud.com/conf-2020/" >}}, a bi-annual conference for {{< target-blank "Nextcloud" "https://nextcloud.com/" >}} enthusiasts. Nextcloud is an open source collaboration software for file sharing and storage, collaborative editing, calendar, and (lots of) other team apps — and it is the software we use to build our <a href="/products/">LaceWing Cloud</a>. As CTO of LaceWing Tech, it’s important to me to connect and learn from the community - and that I did!

If you missed the conference, checkout my talk "Security demands Usability" (20 mins) on {{< target-blank "YouTube" "https://youtu.be/H3vWeqrWnoQ?t=11058" >}}, where I discuss LaceWing Tech's holistic approach to security. Many things, not just firewalls or VPNs, can affect one’s data security — lack of training, stress on the job, unclear processes, or even unorganized data. This is why it is important for organizations to look at the bigger picture, and that means understand the user experience as an important part of security.

Me in action:

{{< tweet 1312372266286673920 >}}

## Highlights

If you’d like to catch up on other talks you missed at Nextcloud Conf, here are my top three:

1. Keynote by Nextcloud CEO Frank Karlitschek and Nextcloud Design Lead Jan Borchardt (long - 1,5 hours). In this “state of the union”, Frank and Jan list 10 areas of new exciting features on Nextcloud 20. This is a must-see for anyone working closely with Nextcloud, especially IT Admins. {{< target-blank "Part 1" "https://youtu.be/8J4XDIILcgg?t=47" >}} and {{< target-blank "Part 2" "https://www.youtube.com/watch?v=H3vWeqrWnoQ&feature=youtu.be&t=37" >}}.

2. {{< target-blank "Security by Nextcloud CEO Frank (20 mins)" "https://youtu.be/H3vWeqrWnoQ?t=15649" >}} - overview of Nextcloud security features, including password policies, remote wipe, file access control and encryption. Frank also gives an overview of how Nextcloud ensures quality (and therefore security) - for example, they have a 10k bug bounty on {{< target-blank "Hackerone" "https://hackerone.com/nextcloud" >}} and they haven’t had any reports yet!

3. {{< target-blank "How Nextcloud saved our work from Covid-19 (5 mins)" "https://youtu.be/PtnbAf8uQ-M?t=22074" >}}. When the covid crisis hit, this small municipality in Italy migrated to Nextcloud, saving them time and lots of headaches. In this short talk, Michele Bordi, a system administrator from Comune di Macerata, tells their migration story from a Windows server and ~25 users working remotely 1 day per week to a Nextcloud server and ~250 remote users 6 days per week. It’s quite the journey! Take a listen.

Honorable mentions - the talk by {{< target-blank "ONLYOFFICE (5 mins)" "https://youtu.be/PtnbAf8uQ-M?t=8785" >}} provides a great overview of OnlyOffice’s new features, including version history (our new favorite feature at LaceWing Tech). And for software developers, Jan Borchardt’s {{< target-blank "accessibility tips (5 mins)" "https://youtu.be/H3vWeqrWnoQ?t=17953" >}} are priceless!

## Learnings

1. Integrations and partnerships - Nextcloud announced a new initiative called Nextcloud Platform, which makes it easier to integrate 3rd-party services and build new apps on top of Nextcloud. Along with LaceWing Tech, Nextcloud understands that some organizations might need to migrate step by step from Google Drive or Microsoft’s OneDrive. They have built new migration tools and allow one to mount Google Drive or OneDrive folders directly into Nextcloud. Of course the goal is 100% migration to Nextcloud, but in the meantime, these integrations can enable a more stable, appropriately paced migration, depending on your organization’s needs. As LaceWing provides [onboarding and migration support](/products/), this is a big help for us making Nextcloud more accessible.

2. Lots of new features in Nextcloud 20 - status updates, fancy dashboard with lots of customizable widgets, design makeover for the Deskstop client, an improved unified search across all apps, and more fine-grained control over notifications. See the keynote for an overview. LaceWing Tech is still testing Nextcloud 20 to make sure these features are “finished enough” and plan to release some of them soon.

3. Huge community = huge results (i.e., :thumbs_up: [open source](/blog/why-is-open-source-important/)). I was inspired to see how many developers are building new apps and continuously improving Nextcloud. This large community and customer base shows that Nextcloud is likely to improve and stay relevant for years to come.

Thanks again to Nextcloud GmbH for building great software and also for building a lovely community! See you at the next conference - whether online or IRL :wave:.
