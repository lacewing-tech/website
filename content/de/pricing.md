---
draft: false
headline: Preise
subtitle: ""
url: preise
linkTitle: Preise
title: Preise
date: 2020-06-19T13:41:56.335Z
images:
weight: 2
description: Hier erfährst du, wie unsere Preise für Cloud-Storage, Consulting
  und IT-Support aussehen und von welchen Faktoren sie abhängen.
menu: 
---
Unsere Leistungen werden an eure individuellen Bedürfnisse angepasst. Somit hängen unsere Preise von vielen Faktoren ab wie

* Datenvolumen
* IT-Support Bedürfnisse
* Anzahl der Mitarbeiter*innen (Cloud-Nutzerinnen)

Da wir einen hochqualitativen Rundumservice bieten, können viele der Preisfaktoren erst nach einem ersten Beratungsgespräch geklärt werden.

## Du willst eine persönliche Preisangabe?

Setze dich bitte [per Kontaktformular](/de/kontakt/) mit uns in Verbindung, damit wir dir nach einem ersten kostenlosen Beratungsgespräch (ca. 30 Minuten) ein genaues Angebot machen können.

### Preisbeispiele zur Orientierung

Hier stellen wir dir zwei fiktive Beispiele mit potentiellen Preisen vor:

#### **Firma A mit fünf Mitarbeiter*innen**

Eine Firma mit fünf Mitarbeiter*innen benötigt eine sichere und nachhaltige Cloud-Lösung, um firmeninterne Daten zu speichern und zu teilen.

Sie arbeitet mit einem Datenvolumen bis zu 300GB täglich, braucht also regelmäßige Backups dafür. Zusätzlich möchte Firma A 1TB Cloud-Speicherung ohne Backup für ihr Archiv. Firma A braucht bis zu fünf Stunden IT-Support pro Monat, um jegliche Probleme und Fragen rund um die Cloud und alle anderen IT-Bedürfnisse zu klären.

Um mit der LaceWing Cloud souverän arbeiten zu können, bucht Firma A einen Workshop über die Nextcloud für ihre Mitarbeiter*innen.

Firma A bezahlt 699€ im Monat für die folgenden Dienstleistungen:

* LaceWing Cloud mit regelmäßigen Backups bis zu 300GB
* LaceWing Cloud für das Archiv ohne Backups: bis zu 1TB
* IT-Support bis zu 5 Stunden im Monat zu allen IT-Themen
* Workshop zum Thema Nextcloud für 5 Mitarbeiter*innen

- - -

#### **Firma B mit 50 Mitarbeiter*innen**

Eine Firma mit 50 Mitarbeiter*innen benötigt eine sichere und nachhaltige Cloud-Lösung, um firmeninterne Daten zu speichern und zu teilen.

Sie arbeitet mit einem Datenvolumen von 3TB täglich, braucht also regelmäßige Backups dafür. Zusätzlich möchte Firma B 10TB Cloud Speicherung ohne Backup für ihr Archiv. Firma B hat viele Mitarbeiter*innen und benötigt dementsprechend mehr IT-Support. Sie bucht also bis zu 30 Stunden IT-Support pro Monat, was alle IT-Fragen von ihren Mitarbeiter*innen und die proaktive Serverwartung decken soll.

Um mit der LaceWing Cloud souverän arbeiten zu können, bucht Firma A einen Workshop über die Nextcloud für ihre Mitarbeiter*innen.

Firma B bezahlt 4.099€ im Monat für die folgenden Dienstleistungen:

* LaceWing Cloud mit regelmäßigen Backups: bis zu 3TB
* LaceWing Cloud für das Archiv ohne Backups: bis zu 10TB
* IT-Support bis zu 30 Stunden im Monat zu allen IT-Themen
* Workshop zum Thema Nextcloud für 50 Mitarbeiter*innen
