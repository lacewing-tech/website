---
headline: Goodbye, Zoom! Open Source Videokonferenz-Tools im Vergleich
subtitle: ""
url: blog/open-source-video-konferenz-tools-vergleich
title: Goodbye, Zoom! Open-Source Videokonferenz-Tools im Vergleich
description: Welches sind gute Open Source Video Konferenz Tools? Unser kleiner
  Ratgeber hilft dir bei der Auswahl.
author: Laura Wadden
date: 2020-08-14T11:44:04.252Z
---
Bei LaceWing Tech haben wir dasselbe Problem wie alle anderen auch - wie treffen wir uns online auf eine nachhaltige, benutzerfreundliche und doch sichere Art und Weise? Wir haben einige Monate damit verbracht, verschiedene Tools zu testen und uns schließlich für **[Jitsi-Meet](https://jitsi.org/jitsi-meet/)**. entschieden. Es ist Open Source, einfach zu benutzen und hat alle Funktionen, die man für die tägliche Arbeit und Meetings braucht. In diesem Artikel erkläre ich euch, warum wir Jitsi-Meet schätzen und wie wir bei LaceWing Tech Online-Meetings abhalten.

Lesezeit: ca. 2 Minuten

### Die Alternativen

Es gibt nicht so viele Open Source Videokonferenz-Alternativen und nur zwei sind wirklich gut: **Jitsi-Meet und** [BigBlueButton](https://bigbluebutton.org/) (siehe Tabelle, credits: Swecha).

![Tabelle: Vergleich diverser Video-Chat-Alternativen. BigBlueButton, Jitsi, Microsoft teams, Cisco webex, Zoom and Google Meet. Grafik: Swecha Comparison of various video chat alternatives:](/images/uploads/comparison-various-video-conferencing-tools-swecha.jpg "Video-Koniferenz-Tools im Vergleich. Foto: Swecha")

Beide sind Open Source und benutzerfreundlich, haben aber in unterschiedlichen Bereichen ihre Stärken. BigBlueButton eignet sich am besten für Präsentationen und Veranstaltungen, weil es leistungsfähiger, stabiler und mit mehr schicken Funktionen ausgestattet ist - aber es benötigt viel mehr Ressourcen. Jitsi-Meet ist perfekt für die tägliche Arbeit, weil es simpel ist, genau das bietet, was wir brauchen, und weniger Ressourcen verbraucht.

### Andere Tools die von uns getestet wurden:

* [Wire](https://wire.com/en/): Wir verwenden die kostenlose Wire-Version für unseren Team-Chat und 1-1 Videoanrufe. Wir haben das Wire-Produkt "Teams" für Video-Chats mit mehreren Personen getestet, aber es hatte nicht genügend zusätzliche Funktionen, um die Kosten zu rechtfertigen.
* [Nextcloud Talk](https://nextcloud.com/talk/): Wir raten von der Verwendung von Nextcloud Talk (noch!) ab, da es leider unbenutzbar ist. Wir werden die Tests fortsetzen, sobald sich das Tool verbessert hat.

### Jitsi-Meet mit einem 10-köpfigen Team - es funktioniert!

Für die tägliche Arbeit von LaceWing Tech hat Jitsi-Meet alles, was wir brauchen: Screensharing, Audio und Video stumm- bzw. ausschalten, Chat, "Hand heben", Kachelansicht (als Alternative zur Sprecher_innenansicht) und mehr.

Ich habe etwa 4-5 Gespräche pro Tag, meistens auf Jitsi, und habe selten Probleme. Ich erlebe vielleicht das unvermeidliche "Kannst du mich sehen? Kannst du mich hören?" 2-3 Minuten lang, aber danach geht alles relativ glatt. Auch Gespräche mit mehr als 8 Personen sind kein Problem. Das Setup ist einfach und die Benutzeroberfläche ist leicht zu bedienen - ich kann einer Person einfachen einen Link schicken und sie/er kann über den Browser oder ihr/sein Smartphone sofort teilnehmen.

Schaut euch diese zufriedenen Menschen an: 

![Vier Menschen freuen sich über einen Video-Call!](/images/uploads/winner-2.png "LaceWing Tech in einem Jitisi-Meet-Video-Chat")

Wenn jemand Internetprobleme hat, ist die Situationen jedoch immer noch sehr schwierig. Unser professioneller IT-Tipp? Beschwöre den Computer und bete. Ansonsten versuche ich, für wichtige Treffen einen Notfallplan parat zu haben, so dass, wenn wir Pech haben, alle wissen, was die nächsten Schritte sind.

Jitsi leistet großartige Arbeit, wenn es darum geht, den Ton konstant zu halten, während das Video möglicherweise verzögert wird - wenn also das Video einfriert, könnt ihr die Sitzung trotzdem ohne Probleme ablaufen lassen. Manchmal schalten wir unser Video aus, um die Qualität zu verbessern. Jitsi-Meet unterstützt problemlos 10 Benutzer\*innen gleichzeitig, 20 sind möglich, und 50 werden als möglich gemeldet, erfordern aber wahrscheinlich eine ziemlich fein abgestimmte Server-Konfiguration. Obwohl Jitsi-Meet noch nicht für mehr als zwei Teilnehmer\*innen durchgängig verschlüsselt ist, werden bereits funktionierende Prototypen bereitgestellt und getestet.

Die Usability/User Experience hängt von einer Kombination aus Zuverlässigkeit der Internet-Verbindung des Clients und der Server-Einrichtung ab. Nicht alle Jitsi-Server (z.B. https://meet.systemli.org) sind gleichermaßen gut aufgebaut, daher **vielen Dank an** [Systemli](https://www.systemli.org), **ein Berliner Technologie-Kollektiv und Freund*innen von uns**, für die Bereitstellung einer solch stabilen Umgebung! Hut ab vor dem Jitsi-Meet-Team für den Aufbau und die Wartung einer benutzerfreundlichen Alternative zum 2-Milliarden-Dollar-Zoom. Das ist nicht einfach, besonders wenn größere Unternehmen unbegrenzt Geld für User Experience (UX)-Designer*innen ausgeben können. 

### Wie geht es weiter? Jitsi-Meet von LaceWing Tech

Zukünftig wird LaceWing Tech Jitsi-Meet als Produkt anbieten.  [Abonniere unseren Newsletter](#newsletter), um die neuesten Updates zu erhalten.