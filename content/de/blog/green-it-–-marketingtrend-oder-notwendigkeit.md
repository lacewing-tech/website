---
headline: "Green IT: Marketingtrend oder Notwendigkeit?"
subtitle: ""
url: blog/green-it-marketingtrend-oder-notwendigkeit
title: Green IT – Marketingtrend oder Notwendigkeit?
description: Hier erklären wir euch, was Green IT ist und warum wir sie
  brauchen. Nützliche Tipps, die ihr direkt umsetzen könnt, gibt es dazu!
author: Senta Brockmann
date: 2020-06-19T14:59:46.660Z
---
Ökostrom, nachhaltige und faire Mode, Bio-Essen – der Trend zum umweltbewussten Handeln und Konsumieren ist längst in alle möglichen Lebensbereiche vorgedrungen. Das auch IT grün, nachhaltig und fair sein kann, haben dabei die wenigsten auf dem Schirm. Doch seit ein paar Jahren gerät auch das Thema Green IT immer mehr in den Fokus von Verbraucherinnen und Unternehmen.

Hier erklären wir euch, was Green IT ist und warum wir Green IT brauchen. Ihr erfahrt außerdem was der Unterschied zwischen Green in IT und Green through IT ist. Zum Schluss zeigen wir euch, was wir als Verbraucherinnen oder Unternehmen machen können, um unsere IT nachhaltiger zu gestalten und warum IT und Nachhaltigkeit kein Widerspruch ist.

Lesezeit: ca. 13 Minuten

Foto: Floris Andréa // [Unsplash](https://unsplash.com/photos/V623FFNdCmI)

![Green IT](/images/uploads/green-it-foto-floris-andrea-unsplash.jpg " Green IT - Marketingtrend oder Notwendigkeit?")

## Was ist Green IT?

Green IT bedeutet nichts anderes als ein nachhaltiger, ressourcenschonender, effizienter und damit umweltfreundlicher Umgang mit IT-Produkten und IT-Dienstleistungen. Das fängt bei der Herstellung von IT-Produkten an, geht über die Nutzung der Produkte im Alltag bis hin zur Entsorgung. Auch der Einsatz von IT zur Vermeidung von einem unnötigen Ressourcenverbrauch gehört zu Green IT dazu. Es geht neben der Nutzung von Laptops und Smartphones etc. auch um Server, Websites und Software, die nachhaltig produziert oder eingesetzt werden können. Ein weiterer wichtiger Punkt ist die Sensibilisierung für Green IT, also das Bewusstsein dafür zu erschaffen, dass jeder etwas dazu beitragen kann den Einsatz von IT effizienter und ressourcenschonender zu gestalten.

Bereits im Jahr 2008 hat die Bundesregierung den Aktionsplan „Green IT“ vorgestellt, um Deutschland zum Pionier in Sachen Green IT werden zu lassen (Vgl. dazu [cio.bund.de](https://www.cio.bund.de/Web/DE/IT-Beschaffung/Nachhaltigkeit-in-der-IT-Beschaffung/Nachhaltigkeit-in-der-IT-Beschaffung_node)). Seit 2019 gilt bei der Bundesregierung eine nachhaltige und umweltfreundliche IT-Beschaffungsstrategie. Dort geht es unter anderem um die Beschaffung energieeffizienter Produkte und Dienstleistungen und die Berücksichtigung des Umweltzeichens „Blauer Engel“. Auch hier gibt es also mittlerweile ein erhöhtes Bewusstsein dafür, das IT auch grün sein kann.

### Warum brauchen wir Green IT?

Immer mehr Menschen nutzen das Internet, egal ob privat oder beruflich. Die dafür nötigen technischen Geräte sind aus unserem Alltag nur noch sehr schwer wegzudenken. Doch was heißt das für unsere Umwelt? Wie viel ist am Widerspruch zwischen Nachhaltigkeit und IT tatsächlich dran. Bei der Herstellung von Laptops, Smartphones und Co. werden seltene Mineralien verarbeitet, die unter prekären sozialen Bedingungen in Minen abgebaut werden. Gleichzeit findet die Produktion von IT-Hardware fast ausschließlich unter nicht umweltfreundlichen Bedingungen.

Bei jeder einzelnen Suchanfrage im Internet werden CO2 Emissionen produziert und Strom verbraucht. Im Endeffekt verbraucht jede einzelne Website, die aufgerufen wird Strom und Ressourcen. Außerdem müssen dafür jederzeit unendlich viele Server in unendlich großen Serverfarmen weltweit bereitstehen, die ebenfalls Strom verbrauchen und den ganzen Tag gekühlt werden müssen. Wird ein Laptop oder ein Smartphone nicht mehr genutzt, landet das Gerät irgendwann als Elektronikabfall auf dem Müll. Von der Verpackung der Geräte beim Kauf ganz zu schweigen.

Damit dieser Kreislauf in Zukunft weniger Strom verbraucht, weniger Müll produziert, weniger CO2 Emissionen herstellt und auch weniger Mineralien abgebaut werden müssen, brauchen wir das Bewusstsein dafür, das Green IT möglich und nötig ist. Wie und auf welchen Ebenen Green IT möglich ist, zeigen die beiden verschiedenen Aspekte – Green in IT und Green through IT.

### Der Unterschied zwischen Green in IT und Green through IT

Der Unterschied zwischen Green in IT und Green through IT ist einfach erklärt:  Green in IT meint, wie die Verwendung von IT selbst nachhaltig gestaltet werden kann und Green through IT meint, was durch die Nutzung von IT nachhaltiger wird.

Bei Green in IT wird also geschaut, was innerhalb von vorhandenen IT-Strukturen und Hardware fairer und nachhaltiger werden kann. Das Ziel ist es, den Bedarf an Ressourcen und Energie für die Nutzung von IT so weit wie möglich zu minimieren. Zum Beispiel durch die Nutzung von Cloud Computing, indem Speicherplatz auf einem bestehenden Server gemietet wird, statt einen eigenen Server dafür zu nutzen, der gekauft werden muss und Strom verbraucht.

Green through IT zeigt dagegen auf, wie durch den Einsatz von IT weniger Ressourcen und weniger Energie verbraucht werden kann. Anstatt eine Flugreise zu buchen um Kundinnen vor Ort zu treffen, können Businesstreffen auch per Telefon- oder Videokonferenz stattfinden und verbrauchen damit deutlich weniger Ressourcen als der Hin – und Rückflug. Auch durch das Versenden von Rechnungen oder Newsletter per Mail statt per Post können Ressourcen eingespart werden. Wie und wo Verbraucherinnen und Unternehmen Green IT umsetzen können?

Wer sich jetzt fragt, wie er oder sie als Verbraucherin oder Unternehmerin selbst IT nachhaltiger umsetzen kann, bekommt hier von uns einige praktische Tipps und Hinweise.

#### 1. Sensibilisierung für Green IT

Das Wichtigste ist zuallererst die Sensibilisierung dafür, dass IT Ressourcen verbraucht und jede Suchanfrage oder jeder neue Laptop oder jedes neue Smartphone bereits Ressourcen verbraucht haben. Wem dann im zweiten Schritt klar wird, dass auch IT grün, also nachhaltig und umweltbewusst sein kann, der/die wird auch in Zukunft mehr darüber nachdenken wie das in seinem Alltag möglich ist.

#### 2. Nutzung von Open Source Software

Egal, ob Mailprogramme, Browser oder Textverarbeitung – jede Software, die genutzt wird, verbraucht nicht nur Rechenleistung sondern auch Speicherkapazität. Und je neuer die Software, desto leistungsfähiger muss in der Regel auch die Hardware sein. Ein scheinbar unendlicher Wettlauf um immer höhere Speicherkapazität und um immer bessere Rechenleistung entsteht.

Dass das nicht so sein muss, zeigt der Einsatz von [Open Source Software](/blog/why-is-open-source-important/).  Unter Open Source Software versteht man eine Software, deren Quellcode für alle frei zugänglich, veränderbar und zudem meist kostenlos ist. Darunter fällt z.B. der Browser Firefox, das Mailprogramm Thunderbird und das OpenOffice. Zudem ist die Software modular aufgebaut und mit Schnittstellen versehen. Das heißt, ich kann nur den Teil der Software nutzen und ggf. verändern und optimieren, den ich wirklich benutze. So wird so wenig Software wie nötig eingesetzt, was den Verbrauch von Ressourcen senkt und den Einsatz von Open Source Software somit nachhaltiger macht, als den Einsatz von sog. Closed Source Software. Die kann nämlich weder verändert noch modular eingesetzt werden und gibt es nur im Gesamtpaket.

#### 3. Nachhaltige Websites bauen

Auch Websites können nachhaltig gebaut werden. Wie das gehen soll? Damit Inhalte schneller auf einer Website gefunden werden, müssen bei einer nachhaltigen Website die Inhalte durch intelligentes SEO und den Einsatz von Keywords schnell auffindbar gemacht werden. Dazu gehört auch, dass die Seite intuitiv und übersichtlich bedienbar ist, also die Usability (Benutzerfreundlichkeit) beachtet wird. Denn je länger wir auf einer Website surfen und Dinge suchen, umso mehr Ressourcen verbrauchen wir.

Ein weiterer Punkt ist die Nutzung von statischen Websites. Die benötigen im Gegensatz zu dynamischen Websites weniger Ressourcen und weniger Rechenleistung, weil deren Ladezeiten viel geringer sind. Das liegt daran, dass die einzelnen Seiten einer Website bei einer statischen Website nur dann geladen werden, wenn sie auch angeklickt werden. Und dadurch laden die Seiten auch schneller.

Ein CMS (Contentmanagement-Systeme) zum Bauen von statischen Websites ist zum Beispiel Hugo. Dynamische Websites, wie beispielsweise alle Wordpress-Seiten, brauchen dagegen Datenbanken und werden immer komplett geladen. Das verbraucht mehr Strom und benötigt mehr Server- und Rechenleistung. Je länger eine Website aufgrund ihrer Inhalte zum Laden braucht, umso mehr Ressourcen werden verbraucht.

#### 4. Nachhaltige IT-Beschaffung

Wer sich einen neuen Laptop oder ein neues Smartphone kaufen möchte, ist bereits mit einer Vielzahl von Kriterien beschäftigt, die das neue Gerät können und haben muss. Das Thema Nachhaltigkeit spielt dabei so gut wie keine Rolle. Auch, weil es bisher kein einziges wirklich faires oder nachhaltige Produkt in diesem Bereich zu kaufen gibt. Auch wenn das Fairphone der gleichnamigen Firma aus den Niederlanden schon einen guten Anfang gemacht hat, gibt es in diesem Markt noch vieles zu verbessern. Diese 5 Fragen kannst du dir beim nächsten Einkauf von Hardware stellen oder deiner/deinem Händler*in des Vertrauens stellen:

1. Wurde das Produkt energieeffizient produziert und ist das Produkt selbst energieeffizient?
2. Ist das Produkt recyclebar?
3. Wieviel Verpackung wurde genutzt?
4. Kann das Produkt aufgerüstet werden? Also ist zum Beispiel der Speicherplatz modular erweiterbar?
5. Kann das Produkt repariert werden? Oder komme ich an die wichtigen Teile gar nicht heran? Damit werden nicht nur die Händlerinnen, sondern auch nach und nach die Produzentinnen auf das Bedürfnis ihrer Kund*innen nach Green IT aufmerksam.
6. Einsatz von Thin Clients

Für Unternehmen ist der Einsatz von Thin Clients eine einfache Möglichkeit Ressourcen zu sparen. Doch was sind Thin Clients eigentlich? Thin Clients sind Computer, auch als Mini-PCs, Notebooks oder Netbooks bezeichnet, die über eine Netzwerkverbindung mit einem Server verbunden sind und dessen Ressourcen nutzen. Sie haben keine eigene Festplatte und eine geringe Rechenleistung, da diese von der Serverseite gestellt wird.

Konfiguriert und verwaltet werden sie zentral über eine Verwaltungssoftware. Dadurch benötigen diese Computer im Gebrauch weniger Strom und bei der Herstellung weniger Material. Ausgestattet mit Maus, Monitor, Tastatur und Sound sind Thin Clients für die üblichen Office-Anwendungen und für die Arbeit mit dem Internet völlig ausreichend und damit eine ideale nachhaltige Alternative zu klassischen Laptops oder Computern.

#### 6. Reusing und Refurbishing von Hardware

Ein gebrauchter Laptop? Ein second-hand Smartphone? In der IT-Welt ist das zum Teil nichts Ungewöhnliches und nennt sich Reusing und Refurbishing. Dabei werden jedoch die Geräte nicht einfach nur ein zweites Mal genutzt (Reusing), sondern vorher professionell wiederaufbereitet (Refurbishing).

Denn was wir oft schon als Elektroschrott deklarieren ist oft noch ziemlich gut brauchbar oder gar wertvoll. Viele gebrauchte Laptops sind von Unternehmen gekauft und nie oder nur selten genutzt worden. Meistens sind es hochwertige Leasing-Geräte oder professionelle Produkte, die durch das Widerrufsrecht als Retoure wieder an die Hersteller zurückgeschickt wurden. Beim Refurbishing werden die Geräte gründlich untersucht, gereinigt und die Festplatten unter der Einhaltung bestimmter Richtlinien gelöscht. Qualitätseinschränkungen bei der Wiederverwendung der Geräte gibt es gegenüber neuen Geräten nicht.

Nachhaltig ist das System so oder so. Neuanschaffungen werden nach hinten verschoben oder verringert. Es entsteht weniger Elektronikschrott und die Ressourcen, die zur Produktion von neuen Geräten genutzt worden wären, fallen nicht an. Das Reusing und Refurbishing ist also nicht nur auf nachhaltiger, sondern auch auf sozialer Ebene absolut ein Gewinn. Verschiedene Plattformen im Internet bieten Refurbished-Hardware mittlerweile sowohl für Unternehmen als auch für Verbraucher*innen an.  

#### 7. Cloud Computing

Jede Menge Dateien und Fotos, viele große Präsentationen, unendlich viele Mails mit großen Anhängen und schon ist nicht nur die Festplatte des Laptops, sondern auch schnell der Speicherplatz des Email-Anbieters mehr als voll. Und woher bekomme ich eigentlich die Datei der Kollegin, wenn ich im Homeoffice arbeite? Beim Cloud Computing geht es nicht nur um Speicherplatz, sondern um ein effizienteres und kollaboratives Arbeiten. Der Austausch von Dateien findet nicht mehr per Mail statt, sondern über eine Ablage in der Cloud. Darauf können dann alle im Team zugreifen, ohne Speicherplatz auf ihrer Hardware selbst zu benötigen.

Auch Funktionen und Programme, wie zum Beispiel Kalender oder Projektmanagement-Software, werden in die Cloud ausgelagert und können dort von allen benutzt werden. Alle erdenklichen Dateien wie Fotos, Videos, Tabellen usw. können in der Cloud strukturiert gespeichert, geteilt und bearbeitet werden und die doppelte Datenspeicherung, die nicht nur Speicherplatz, sondern auch Strom verbraucht, gehört der Vergangenheit an.

Dabei werden allerdings nur die Speicherkapazitäten zwischen verschiedenen Unternehmen oder Verbraucherinnen geteilt, nicht jedoch die Daten an sich. Besonders nachhaltig ist das Cloud Computing durch den Wegfall eines eigenen Servers, der dann nicht mehr gebraucht wird. Der Server der Cloud-Anbieterinnen wird durch die Nutzung von verschiedenen Unternehmen und ihren Mitarbeiter*innen optimal ausgelastet. Außerdem werden die eigenen Geräte durch die Nutzung der Cloud nicht so stark beansprucht. Die gespeicherten Daten und Programme in der Cloud können von überall aus flexibel abgerufen werden.

Für die effiziente und einfache Arbeit im Homeoffice ist die Nutzung einer Cloud ideal. Damit ist die Cloud nicht nur Green IT, sondern auch Green through IT.

### Fazit

Um den eigenen digitalen Fußabdruck etwas kleiner machen gibt es mittlerweile mehr als genug Möglichkeiten für Unternehmen, aber auch Verbraucher*innen. Auch wenn es immer noch nicht möglich ist komplett nachhaltige Geräte zu kaufen, haben wir alle die Möglichkeit das Thema Green IT in unseren Alltag zu integrieren. Green IT ist einfach mehr als nur darauf zu achten, dass die Geräte nicht bei Stand-by weiterlaufen und auch kein gehypter Marketingtrend. Green IT ist eine Notwendigkeit, die allen bewusst sein sollte.
