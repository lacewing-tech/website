---
draft: false
headline: Pricing
subtitle: ""
url: pricing
linkTitle: Pricing
title: Pricing
date: 2020-06-22T13:32:07.259Z
images:
weight: 2
description: Our pricing depends on many factors, like how much data you need to
  store, IT support needs, number of cloud users, etc.
menu: 
---
As our services are tailored to your company’s individual needs, our pricing depends on many factors, like how much data you need to store, your IT support needs, the number of employees (cloud users) you have, and so on. Because our offering is a high-quality all-round service, we first need to hold an individual consultation with you.

## You’d like a personal price quotation?

Please get in touch with us via the [contact form](/contact/) so that we can make you an exact offer, with a free initial consultation (approx. 30 minutes).

### Pricing examples

Here we present two examples with potential prices:

#### Company A with five employees

A company with five employees needs a secure and sustainable cloud solution to store and share internal company data.

It has about 300GB of data that’s worked with daily, so this data needs to be regularly backed up. Additionally, Company A wants 1TB cloud storage without backups for their archives. Company A needs up to five hours of IT support per month to resolve any problems and questions about the cloud services, and all of their other IT needs. To be able to work confidently with the LaceWing Cloud, Company A additionally books a Nextcloud workshop for their employees.

Company A pays 699€ per month for the following services:

* LaceWing Cloud with regular backups up to 300GB
* LaceWing Cloud for the archive without backups: up to 1TB
* IT support up to 5 hours per month on all IT topics
* Workshop on the topic of Nextcloud for 5 employees

- - -

#### Company B with 50 employees

A company with 50 employees needs a secure and sustainable cloud solution to store and share corporate data.

It works with about 3TB of data daily, so this data needs to be regularly backed up. Additionally, company B wants 10TB cloud storage (without backups) for their archive. Company B has many employees and therefore needs more IT support. They book up to 30 hours of IT support per month, which should cover all IT issues from their staff, as well as proactive server maintenance.

To be able to work confidently with the LaceWing Cloud, company A books a Nextcloud workshop for their employees.

Company B pays 4.099€ per month for the following services:

* LaceWing Cloud with regular backups: up to 3TB
* LaceWing Cloud for the archive without backups: up to 10TB
* IT support up to 30 hours per month on all IT topics
* Workshop on the topic of Nextcloud for 50 employees
