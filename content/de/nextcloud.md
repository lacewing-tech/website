---
title: "Was ist Nextcloud? Und ist Nextcloud das Richtige für mein Team?"
date: 2020-11-02T08:23:41+01:00
draft: false
headline: "Ist Nextcloud die richtige Cloud-Software für mich und mein Team?"
subtitle: "Eine Entscheidungshilfe für Teams und Unternehmen in 10 Fragen & 10 Antworten"
url: produkte/nextcloud
linkTitle: Nextcloud
images:
- /images/uploads/Nextcloud_Kollaboratives Arbeiten_1200.jpg
weight: 0
description: "Nextcloud: 10 Fragen und 10 Antworten zu Verschlüsselung, Datenschutz, Funktionen und Apps. Alles was Du und Dein Team über die Cloud-Software wissen müssen."
menu:
toc: true
---

## 1. Was ist Nextcloud?
{{< target-blank "Nextcloud" "https://nextcloud.com/" >}} wird als eine Open-Source-Software, eine Kollaborations-Software, eine File-Sharing-Software und eine Cloud-Software bezeichnet, die besonderen Wert auf Datenschutz legt und DSGVO-konform ist. Und das Beste ist, alles davon stimmt!

- **Open Source**: Nextcloud ist eine Open-Source-Software, weil der Code der Software für alle zugänglich, also für alle offen einsehbar und frei verfügbar ist. Und frei verfügbar heißt  kostenlos.
- **Kollaboration**: Nextcloud ist Kollaborations-Software, weil sie viele Apps und Funktionen anbietet, mit denen einzelne Teams oder auch ganze Unternehmen einfach und reibungslos zusammenarbeiten können. Seit Januar 2020 gibt es Nextcloud Version 18 mit einem integrierten Office-Paket (seitdem heißt es auch Nextcloud Hub).
- **Dateien teilen**: Wie der Titel "File-Sharing-Software" schon sagt, kann man mit Nextcloud unkompliziert Daten miteinander teilen, die auf Nextcloud gespeichert sind. Egal ob im ganzen Team, unter einzelnen Kolleg:innen oder mit externen Dritten. Die Daten werden dabei ständig synchronisiert, und so sind alle immer auf dem neuesten Stand.
- **Cloud-Software**: Der Begriff Cloud-Software beschreibt Nextcloud vielleicht am besten. Denn wie in einer Cloud typisch werden auch bei Nextcloud Speicherplatz und Anwendungssoftware als Dienstleistung verfügbar gemacht. Und das, indem Teams und Unternehmen online von überall darauf zugreifen können. Egal, ob vom Desktop aus, mobil über das Smartphone oder direkt in der Web-App.
- **Datenschutz**: In Sachen Datenschutz überzeugt Nextcloud mit vielen verschiedenen Sicherheitseinstellungen, die alle das Ziel haben, die Daten der Teams und Unternehmen zu schützen. Los geht es mit einer einfachen 2-Faktor-Authentifizierung und darüber hinaus gibt es verschiedene Verschlüsselungsmöglichkeiten bis hin zu automatisierten Sicherheitsprüfungen. Und das alles DSGVO-konform.

Zusammengefasst ist Nextcloud also eine Cloud-Software, die Open Source ist, das Thema Datenschutz nicht nur beachtet, sondern auch konsequent umsetzt und mit der Teams und Unternehmen effizient und kollaborativ arbeiten und Dateien austauschen können. Damit ist Nextcloud die ideale Alternative zu Google Drive und Microsoft 365.

{{< figure src="/images/uploads/Nextcloud_Kollaboratives Arbeiten_780.jpg" title="" alt="">}}

## 2. Wer steckt hinter Nextcloud?
Nextcloud wurde von Frank Karlitschek gegründet und wird von einem Team in Deutschland entwickelt. Karlitschek gründete 2010 zunächst ownCloud mit dem Ziel, eine freie Alternative zu kommerziellen Anbietern von Cloud-Software anzubieten. Nachdem ownCloud eine immer gewinnorientiertere und weniger sicherheitsorientierte Richtung einschlug, verließ Frank Karlitschek 2016 zusammen mit einigen anderen Entwickler:innen das Unternehmen. Keine zwei Wochen später gründete er Nextcloud als eigenständige Abspaltung und veröffentlichte umgehend die Nextcloud Version 9, die auf dem Code von ownCloud basiert. Der Fokus von Nextcloud liegt seitdem auf Datenschutz und kollaborativem Arbeiten und ist weiterhin frei verfügbar. Die Finanzierung von Nextcloud erfolgte zunächst durch die Mitarbeitenden selbst, später über den angebotenen Support für Nextcloud.

Bereits 2018 nahm Nextcloud im lokalen Enterprise File-Sync- und Share-Markt eine führende Rolle ein. Nextcloud wird nicht nur vom eigenen Team weiterentwickelt, sondern auch von der großen Nextcloud-Community. Die ist direkt an der  Weiterentwicklung und Verbesserung von Nextcloud beteiligt und hilft dabei, Nextcloud sicherer und benutzerfreundlicher zu machen.

## 3. Wer nutzt Nextcloud?
Unter den geschätzten 250.000 - 300.000 Installationen von Nextcloud befindet sich seit April 2018 auch die Deutsche Bundesregierung. Die nutzt Nextcloud seitdem für über 300.000 Mitarbeitende in Ministerien und anderen Einrichtungen des Bundes.

Seit 2019 ist bekannt, dass Nextcloud auch von anderen EU-Staaten genutzt werden soll. Frankreich, Schweden und die Niederlande haben sich aus Datenschutzgründen ebenfalls für den Einsatz von Nextcloud in ihren Ministerien und Behörden entschieden.

Nicht nur die ganz Großen, also Staaten, weltweit agierende Unternehmen oder Organisationen, nutzen Nextcloud. Auch kleine und mittlere Unternehmen, Einzelselbstständige oder örtliche Vereine und NGOs arbeiten zunehmend mit Nextcloud, und das ebenso vor allem aus Datenschutzgründen.

## 4. Wie sicher ist Nextcloud?
Um Sicherheit, Datenschutz, DSGVO-Konformität und die volle Kontrolle über die Daten für die Nutzer:innen sicherzustellen,  hat Nextcloud ein umfangreiches Sicherheitskonzept nach Industriestandards entwickelt und umgesetzt.

Für Nutzer:innen bietet Nextcloud:
- verschiedene Sicherheitsstufen zur Datenfreigabe (Passwörter für Ordner und Dateien, individuelle Zugriffsrechte),
- eine Fernlöschung bei Verlust oder Diebstahl von Geräten
- diverse Möglichkeiten zur 2-Faktor-Authentifizierung beim Login.

{{< figure src="/images/uploads/Nextcloud_2 Faktor-Auth_1200.JPG" width="780" title="" alt="Screenshot der Einstellungen von Nextcloud zur 2-Fakt-Auth">}}

{{% accordion number="1" title="Details zu Sicherheitsmaßnahmen, die Nutzer:innen einsetzen können" %}}
Dateien können bei Nextcloud mit bestimmten Personen, Gruppen oder auch externen Dritten geteilt werden. Um die Kontrolle über die Dateien zu behalten, gibt es verschiedene Optionen:

- Passwörter zum Öffnen der Dateien vergeben,
- das Teilen mit einem Ablaufdatum versehen,
- spezielle Zugriffsrechte vergeben (Lesen, Bearbeiten, Teilen) oder
- die Download-Möglichkeit blockieren.

Wer es noch sicherer haben will, gibt die Daten nur mit einer Video-Überprüfung frei. Das heißt, dass zur Freigabe der Datei ein Video-Call nötig ist. Dann wird ein Passwort darüber ausgetauscht und die Datei kann von der anderen Person geöffnet werden.

Beim Verlust oder Diebstahl von Laptop und Co. haben Nutzer:innen die Möglichkeit, sowohl die Verbindung zu Nextcloud als auch die Daten auf den Geräten zu löschen, und das per remote wipe, also Fernlöschung. Dabei loggt man sich in Nextcloud ein und kann in den Nutzer:innen-Einstellungen genau sehen, welche Geräte mit Nextcloud verbunden sind und die Verbindung dort sofort löschen.

Nextcloud kann zudem beim Login mit einer 2-Faktor-Authentifizierung versehen werden. Damit ist das Passwort der erste Faktor der Authentifizierung und zusätzlich ist dann noch ein weiteres Passwort, der zweite Faktor, notwendig, das auf das Smartphone geschickt oder in einer App generiert wird. So wird der unerwünschte Zugriff auf Daten in Nextcloud doppelt geschützt.{{% /accordion %}}
Nextcloud-Admins stehen zusätzlich  umfangreiche  Funktionen zur Verfügung, mit denen sie Nextcloud  verwalten und den Zugriff auf Daten regulieren können. Bei der Weiterentwicklung des Codes verwendet Nextcloud ein strenges 5-stufiges Sicherheitsverfahren. Ein Bug-Bounty-Programm stellt sicher, dass Sicherheitslücken im Code umgehend gemeldet und behoben werden. Um die  Umsetzung der DSGVO in Teams und Unternehmen zu unterstützen, hat Nextcloud eine Compliance-Checkliste und ein Handbuch für Admins herausgegeben.
{{% accordion number="2" title="Details zu Sicherheitsmaßnahmen, die Admins einsetzen können" %}}
Nextcloud nutzt Machine Learning zur Erkennung von verdächtigen Anmeldungen. Das bedeutet, dass bei ungewöhnlichen Logins, zum Beispiel zu einer für Nutzer:innen nicht üblichen Tageszeit oder an einem nicht üblichen Standort, die Person eine Benachrichtigung und Admins einen Eintrag ins Protokoll erhalten. Die Sitzung kann ggf. beendet und sofort ein neues Passwort erstellt werden. Admins haben zudem die Möglichkeit, den Account zu deaktivieren oder die Nutzer:innen dazu zu zwingen, ein neues Passwort zu verwenden.

Admins haben zudem viele weitere umfangreiche Optionen, die hier nur beispielhaft genannt werden sollen: Sie können Passwort-Richtlinien durchsetzen, die beinhalten, welche Eigenschaften Passwörter haben müssen, damit sie verwendet werden dürfen. Sie können die Dateiweitergabe für bestimmte Personen oder Gruppen einschränken oder deaktivieren. Oder auch erzwingen, dass bei der Dateiweitergabe immer ein Passwort und Verfallsdatum eingesetzt wird.{{% /accordion %}}

### Verschlüsselung
Wenn Daten zu oder vom Nextcloud-Server übertragen werden, sind sie mit der branchenüblichen SSL-Verschlüsselung gesichert. Diese Art von Verschlüsselung garantiert, dass die Daten an sich und was sie beinhalten bei der Übertragung für niemanden sichtbar sind. Zudem werden alle Daten auf dem Nextcloud-Server immer verschlüsselt gespeichert. Das heißt, selbst wenn es einen unerwünschten Zugriff auf den Server gibt, können die Daten durch die Verschlüsselung nicht gelesen werden. Bei Nextcloud gibt es weiterhin die Möglichkeit, die Daten so zu verschlüsseln, dass selbst Admins diese nicht lesen können, sondern nur die Personen, die die Daten verschlüsselt haben (Ende-zu-Ende-Verschlüsselung).
{{% accordion number="3" title="Die Qualitätssicherung bei Nextcloud" %}}
Mit einem 5-stufigen Sicherheitsverfahren sorgt Nextcloud für einen sicheren Code seiner Open-Source-Software. Dabei geht es Nextcloud vor allem um die Bereitstellung von Dokumentationen und Sicherheitstrainings, die Analyse von Sicherheitslücken, die Festlegung von Standards für Entwickler:innen, die Durchführung von Sicherheitsüberprüfungen und die Veröffentlichung von Sicherheitspatches.

Durch das Bug-Bounty-Programm von Nextcloud, bei dem jede:r auf Software-Bugs im Code hinweisen und dafür eine Belohnung bekommen kann, wird die Nextcloud-Software ständig von der Community auf Sicherheitslücken überprüft. Die Höhe der Belohnung für eine gemeldete Sicherheitslücke richtet sich nach der Auswirkung auf die Sicherheit des Codes.

Die höchste Belohnung von 10.000 $ für das Melden einer kritischen Sicherheitslücke musste Nextcloud bisher noch nie auszahlen. Im Gegensatz zu proprietärer Software, die zum Beispiel Tresorit, Google oder Microsoft anbieten, muss man Nextcloud nicht einfach vertrauen, dass es sicher ist. Die Sicherheit von Nextcloud kann unabhängig überprüft werden und ist absolut transparent.{{% /accordion %}}

## 5. Was kostet Nextcloud?
Die Software  ist kostenlos und steht auf der Webseite von Nextcloud zum Heruntergeladen zur Verfügung.

### Eigenes Hosting
Wird Nextcloud von Unternehmen selbst gehostet, fallen nur indirekte Kosten wie Hardware, Strom und die Arbeitszeit an, die für die Installation und Wartung von Nextcloud nötig sind. Da die Wartung und Installation jedoch sehr anspruchsvoll sind, muss dies von einer Fachperson übernommen werden. Eine unsachgemäße Wartung, zum Beispiel durch Ignorieren von Sicherheitsupdates, kann zu Sicherheitsproblemen führen.

### Hosting durch einen Hosting-Anbieter
Eine andere Möglichkeit ist, Nextcloud von einem externen Partner hosten zu lassen. Die Installation und Wartung von Nextcloud ist dann in der Regel eine kostenpflichtige Dienstleistung und wird auch als Support bezeichnet. Wird Nextcloud auf einem Server vor Ort beim Unternehmen gehostet, spricht man von einer Private Cloud. Ist Nextcloud auf dem Server des Hosting-Unternehmens gespeichert, von einer Public Cloud.

Nextcloud selbst bietet kein Hosting an und hat damit auch keinen Zugriff auf die Daten. Allerdings bietet Nextcloud ein kostenpflichtiges Support-Modell für Unternehmen an.

Dazu kommen Nextcloud Apps und Erweiterungen, also externe Software, mit denen Nextcloud um bestimmte Funktionen ergänzt werden kann, die zum Teil kostenlos und zum Teil kostenpflichtig sind.

## 6. Welche Apps und Funktionen bietet Nextcloud?

Egal, ob es um den Datenaustausch, die Teamarbeit oder Kommunikation geht – Nextcloud bietet das ideale Paket für kollaboratives Arbeiten in Teams und Unternehmen an. Und das alles mit einem umfassenden Datenschutz versehen, der sowohl auf dem Smartphone, in der Web-App oder bei der Arbeit mit Nextcloud vom Desktop aus funktioniert.

{{% accordion number="4" title="Apps zum Datenaustausch: Daten Teilen und Synchronisieren" %}}
Das sichere Teilen, Synchronisieren und kollaborative Bearbeiten von Dateien gehört zu den absoluten Basisfunktionen von Nextcloud. Genauer gesagt geht es hier um das Teilen, Freigeben und zeitgleiche Bearbeiten von bestimmten Dateien und Ordnern mit einzelnen Kolleg:innen, anderen internen Teams oder Gruppen oder aber auch externen Dritten, wie zum Beispiel Kund:innen, Partner:innen oder Steuerberater:innen. Die Daten sind dazu in der Nextcloud gespeichert, werden dort permanent synchronisiert und auch von dort abgerufen. Zugriff zu den Daten haben nur die Personen, die eine Freigabe erhalten haben.

Möchte eine Kolleg:in beispielsweise einen Ordner für das ganze Team anlegen, teilt sie diesen mit dem ganzen Team. Sollen einzelne Dokumente in dem Ordner aber nur für bestimmte Personen sichtbar sein, lässt sich auch das einfach einstellen. Sollen alle Kolleg:innen des Teams bestimmte Dateien zwar sehen, aber nicht bearbeiten dürfen, ist auch diese Einstellung problemlos möglich.{{% /accordion %}}
{{% accordion number="5" title="Apps für Teamarbeit: Nextcloud Kanban-Board, Kalender und mehr" %}}
Neben Dateien lassen sich auch Kalender, Kontakte, Decks (Kanban-Boards) und Kontakte in Nextcloud teilen und synchronisieren und machen somit die Zusammenarbeit nicht nur äußerst einfach, sondern auch produktiv.

Besonders beliebt ist das Nextcloud Kanban-Board, das auch Decks genannt wird. Hier können Teams und Unternehmen ihre Projekte und Workflows detailliert abbilden und so die Fortschritte ihrer täglichen Arbeit übersichtlich visualisieren. Bestimmte Aufgaben in einem Projekt werden anhand einer Karte definiert, die dann einzelnen Personen oder Teams zugeordnet und mit zusätzlichen Infos versehen werden können. So können alle den Stand der Dinge im Deck sehen und keine Information oder Deadline geht verloren. Genauso wie bei Dateien ist auch hier eine Freigabe für einzelne Personen oder Teams möglich.{{% /accordion %}}

{{< figure src="/images/uploads/Nextcloud_Deck_Kanban Board.JPG" width="780" title="" alt="Screenshot eines Nextcloud-Kanban-Boards">}}

{{% accordion number="6" title="Apps zur Kommunikation: Meetings, Konferenzen, Screensharing und Co." %}}
Wer im Homeoffice arbeitet, braucht nicht nur den Zugriff auf Daten und Kalender, sondern auch die Möglichkeit, mit seinen Kolleg:innen oder ganzen Teams direkt zu kommunizieren. Dafür gibt es Nextcloud Talk. Mit diesem Tool ist es möglich, über Nextcloud Meetings und Konferenzen online abzuhalten oder auch den Bildschirm zu teilen.{{% /accordion %}}

### Integration von Outlook und Thunderbird
Die Zusammenarbeit von Nextcloud mit Outlook oder Thunderbird ist denkbar einfach und sorgt dafür, das große Dateianhänge und verstopfte Postfächer der Vergangenheit angehören. Dazu stellt Nextcloud eine Erweiterung für Outlook und Thunderbird zur Verfügung, mit dem die Daten, die bei Nextcloud gespeichert sind, ganz einfach per Link in einer Mail verschickt werden können. Die Daten bleiben weiterhin bei Nextcloud gespeichert und können durch den Link abgerufen werden. Selbstverständlich kann der Link mit einem Passwort und einem Ablaufdatum versehen werden.

Doch nicht nur das Verschicken von Daten, auch das Hochladen von Daten und Dokumenten in Nextcloud von externen Dritten, zum Beispiel von Kund:innen oder Geschäftspartner:innen, funktioniert damit.

Eine weitere Funktion, dass Nextcloud für eine optimale Zusammenarbeit mit Outlook und Thunderbird bereitstellt, ist die Synchronisation von Kalendern, Kontakten und Aufgaben. Das heißt, das Teams und Unternehmen ihre Kalender  und alle dort vorhandenen Funktionen auch in Nextcloud nutzen können.

### Office-Pakete – OnlyOffice und Collabora
OnlyOffice und Collabora Online sind die beiden Office-Pakete, die in Nextcloud integriert und genutzt werden können. Bei beiden Office-Paketen liegen die Daten weiterhin in Nextcloud und sind somit sicher und geschützt vor unerwünschten Zugriffen. Mehr zu den Funktionen erfahrt ihr [hier](#7-welche-office-pakete-gibt-es-für-nextcloud).

### Noch mehr Funktionen? Aber klar doch!
OnlyOffice, Collabora oder Thunderbird sind nur drei der Möglichkeiten, Nextcloud individuell anzupassen. Nextcloud ist modular aufgebaut und lässt sich mit unzähligen Apps und Erweiterungen um viele weitere Funktionen ergänzen.

Alle Apps und Erweiterungen, die LaceWing Tech im Zusammenhang mit Nextcloud anbietet, werden vorher von uns genau geprüft und getestet. Nur die, die uns restlos von ihrer Funktionalität und Sicherheit überzeugen, werden wir auch in unserer Nextcloud anbieten. Wir sorgen dafür, dass die Apps und Erweiterungen reibungslos funktionieren und die Zusammenarbeit von Teams und Unternehmen unterstützen.

Nextcloud entwickelt sich ständig weiter. So wurden auf der {{< target-blank "Nextcloud Conference 2020" "https://nextcloud.com/conf-2020/" >}} wieder viele neue Funktionen und das {{< target-blank "Release der neuesten Version 20" "https://nextcloud.com/blog/nextcloud-hub-20-debuts-dashboard-unifies-search-and-notifications-integrates-with-other-technologies/" >}} vorgestellt.

## 7. Welche Office-Pakete gibt es für Nextcloud?
{{< target-blank "OnlyOffice" "https://www.onlyoffice.com/" >}} und {{< target-blank "Collabora" "https://www.collaboraoffice.com/" >}} sind die beiden Office-Pakete und Kollaborationstools, die Nextcloud zur Bearbeitung von Textdokumententen, Tabellen und Präsentationen anbietet. Mit diesen Tools können Teams ihre Dokumente im Browser zeitgleich bearbeiten.

### OnlyOffice

OnlyOffice ermöglicht die Bearbeitung von Textdokumenten, Tabellen und Präsentationen im Microsoft-Office-Format. Darüber hinaus ist OnlyOffice mit vielen weiteren Modulen ausgestattet und damit viel mehr als nur ein Office-Paket. So gehört beispielsweise ein zentrales Dokumentenmanagement-System dazu, mit dem Teams die Zugriffsrechte auf ihre Dokumente verwalten können.

{{< figure src="/images/uploads/Nextcloud_Onlyoffice_Textverarbeitung.JPG" width="780" title="" alt="Screenshot eines Onlyoffice-Dokuments">}}

### Collabora Online

Collabora Online ist ein Office-Paket, das alle gängigen Office-Formate unterstützt, also Textverarbeitung, Tabellen, Präsentationen etc. Teams können ihre Dokumente, die sie in Nextcloud gespeichert haben, direkt in Collabora öffnen, lesen und bearbeiten und damit auch kollaborativ arbeiten. Eine externe Software ist nicht mehr nötig.

## 8. Wie funktioniert kollaborative Teamarbeit mit Nextcloud?
### Dokumente gemeinsam bearbeiten
Um kollaborativ im Team und/oder Unternehmen arbeiten zu können, gibt es bei Nextcloud die Möglichkeit, Kalender, Aufgaben und Kontakte zu teilen. So ist es kein Problem, gemeinsame Termine zu vereinbaren oder Aufgaben abzustimmen, da Nextcloud jede Änderung sofort synchronisiert und sie damit für alle Mitarbeitenden auch sofort sichtbar ist. Genauso ist es mit den Daten, die auf Nextcloud gespeichert sind: Die Daten können einfach geteilt und zur gleichen Zeit kollaborativ bearbeitet werden. Durch die ständige Synchronisation sind immer alle auf dem neuesten Stand.

### Kanban-Boards
Weitere Funktionen, wie das Kanban-Board von Nextcloud, auch Deck genannt, machen die Zusammenarbeit im Team noch einfacher und effizienter. Hier können Arbeitsprozesse detailliert abgebildet, mit  Infos versehen und Aufgaben bestimmten Personen zugeordnet werden.

### Datenschutz und Zugriffsrechte
Das Thema Datenschutz ist bei all diesen Funktionen in die Arbeitsabläufe integriert, sodass sie ein ganz normaler Teil davon sind. Beim Login sorgt beispielsweise eine 2-Faktor-Authentifizierung für einen zusätzlichen Schutz. Beim Teilen von Daten kann nicht nur genau festgelegt werden, mit wem und für welchen Zeitraum eine Datei mit einer anderen Person und einem ganzen Team oder Kund:innen geteilt wird. Auch welche Zugriffsrechte diese Personen oder Teams haben, kann genau definiert werden: beispielsweise Lesen, Schreiben, Teilen oder Downloaden.  

{{< figure src="/images/uploads/Nextcloud_Dateien Teilen.JPG" width="780" title="" alt="Screenshot von Nextcloud, Ansicht bei Rechtsklick auf eine Datei mit verschiedenen Funktionen zum Teilen der Datei">}}

## 9. Was sind die Vorteile von Nextcloud im Vergleich zu Google und Microsoft?
Im Gegensatz zu Google Drive oder Microsoft 365 ist Nextcloud Open Source. Das heißt, dass der Code der Software für jeden frei einsehbar ist. Das führt zum einen dazu, das etwaige Sicherheitslücken frühzeitig gemeldet und geschlossen werden können. Vor allem die große und sehr aktive Nextcloud-Community ist stark dran beteiligt, Sicherheitslücken im Code ausfindig zu machen und zu melden.

Zum anderen punktet Nextcloud mit einer 100%igen Transparenz, was die Themen Datenschutz und Privatsphäre angeht. Bei einer Open-Source-Software wie Nextcloud ist es nicht möglich, versteckte Hintertüren oder Ähnliches in die Software einzubauen, damit das Software-Unternehmen selbst oder Dritte unerwünscht auf die in Nextcloud gespeicherten Daten zugreifen zu können.

Dazu kommt, dass Teams und Unternehmen selbst entscheiden können, wo sie Nextcloud hosten wollen. Entweder selbst vor Ort auf ihrem eigenen Server oder bei einem Hosting-Anbieter ihrer Wahl.

Das Geschäftsmodell von Nextcloud umfasst eine kostenlose Software und einen kostenpflichtigen individuellen Support für Teams und Unternehmen. Diese können den Service von Nextcloud selbst beziehen oder von anderen auf Nextcloud spezialisierten Unternehmen, wie zum Beispiel LaceWing Tech. Google Drive oder Microsoft 365 bieten so gut wie keinen Support an, verweisen stattdessen auf Foren und sind nicht darauf ausgelegt, Teams und Unternehmen individuell zu unterstützen oder gar persönlich ansprechbar zu sein.


## 10. Wie kann LaceWing Tech Teams und Unternehmen dabei unterstützen, Nextcloud zu nutzen?
LaceWing Tech bietet seinen Kund:innen einen Cloudspeicher inkl. Backups und für die Nutzung von Nextcloud ein unkompliziertes [Rundum-sorglos-Paket](/de/produkte/). Angefangen bei einer individuellen Beratung, die Migration der Daten, Workshops zur Einführung von Nextcloud bis hin zum individuellen Support erhalten unsere Kund:innen bei uns alles, was sie brauchen, um sofort und reibungslos mit Nextcloud arbeiten zu können. Unser Motto lautet: Wir kümmern uns um die Technik, und die Kund:innen um ihre Arbeit.

- **Consulting**: Wir beraten unsere Kund:innen zu Nextcloud, schauen uns die vorhandene Netzwerkinfrastruktur an und erstellen ein individuelles Angebot.
- **Hardware**: Möchten unsere Kund:innen nicht nur mit Nextcloud, sondern auch mit passender, vorkonfigurierter Hardware ausgestattet werden, erhalten sie diese ebenfalls von uns.
- **Implementierung von Nextcloud**: LaceWing Tech installiert Nextcloud nicht nur, sondern passt die Software auch individuell an die Wünsche der Kund:innen an. Dazu gehören ein ausreichend großer Speicherplatz und auch die Migration der vorhandenen Daten von Servern und Festplatten zu Nextcloud. Alle Apps, die wir als Erweiterung von Nextcloud anbieten, sind von uns auf Sicherheit und Datenschutz geprüft.
- **Workshops**: Zur Einführung von Nextcloud erhalten unsere Kund:innen auf Wunsch Workshops, damit sie sofort und möglichst reibungslos mit ihrer Arbeit starten können. Auch zu weiteren sicherheitsrelevanten Themen wie zum Beispiel Passwortmanagement, Virenscannern und Verschlüsselungstechnologien bieten wir unseren Kund:innen Workshops an. Uns ist es wichtig, dass sich unsere Kund:innen mit Nextcloud sicher fühlen und sich auf ihre Arbeit konzentrieren können.
- **Support**: Sollten unsere Kund:innen unsere Hilfe brauchen, sind wir ihre Ansprechpartner:innen und bieten auch langfristig einen persönlichen und individuellen Support. Egal ob es um Backups, Updates, Wartungsarbeiten oder anderes geht. Wir sind für unsere Kund:innen da.

{{% button href="/de/kontakt/" %}}Lass uns sprechen!{{% /button %}}
