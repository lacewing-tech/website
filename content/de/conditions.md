---
draft: false
headline: AGB
subtitle: ""
linkTitle: AGB
title: Allgemeine Geschäftsbedingungen (AGB)
url: agb
date: 2020-06-19T10:00:33.761Z
images:
weight: 0
description: Hier findet ihr unsere allgemeinen Geschäftsbedingungen (AGB) zu
  Leistungserbringung, Verfügbarkeiten, Mängelgewährleistung uvm.
menu: footer
---
## Allgemeine Geschäftsbedingungen (Stand 7/2020)

[Download Stand 07/2020 (PDF)](/072020-AGB-LüdtkeundWadden-LaceWingTechGbR.pdf)
