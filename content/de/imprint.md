---
draft: false
title: Impressum
headline: Impressum
subtitle: ""
url: impressum
linkTitle: Impressum
weight: 2
description: "Hier geht´s zu unserem Impressum. "
toc: false
images: null
date: 2020-06-19T10:27:55.722Z
aliases:
  - /impressum/
menu: footer
---
LaceWing Tech hat seine Arbeit zum 31.12.2020 eingestellt. Bitte lest dazu [unser Statement](/blog/closing/).

Lüdtke und Wadden - LaceWing Tech GbR   
Gesellschafterinnen: Carma Lüdtke & Laura Pierson Wadden

c/o Lüdtke Waldemarstr. 54  
10997 Berlin  

USt-Id-Nr.: DE322298300

E-Mail für allgemeine Anfragen: info @ lacewing.tech  
E-Mail für unsere Kund*innen: support @ lacewing.tech  

Telefonnummer: +49 157 3673 1901  

Wir nutzen das [Hugo-Theme Terrassa](https://themes.gohugo.io/hugo-terrassa-theme/).  

Unsere [Datenschutzhinweise](/de/datenschutz/)
