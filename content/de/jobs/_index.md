---
draft: false
headline: Jobs
subtitle: ""
linkTitle: Jobs
title: Jobs
date: 2020-06-19T14:06:07.816Z
images:
weight: 0
description: From time to time we offer jobs. Find out, what skills we are
  looking for right now.
menu: 
url: jobs
---
Hier findest du unsere Stellenausschreibungen. Übrigens: Wir sind jederzeit für Initiativbewerbungen offen. Melde dich gerne bei uns: [jobs @ lacewing.tech](mailto:jobs@lacewing.tech)

## Offene Stellen
