---
headline: Datenschutzhinweise
url: datenschutz
title: Datenschutzhinweise
date: 2020-06-19T12:46:33.892Z
weight: 1
linkTitle: Datenschutzhinweise
description: Hier findet ihr alles zu unseren Datenschutzbestimmungen.
aliases:
  - /datenschutz/
menu: footer
---
## Information über die Verarbeitung personenbezogener Daten

Im Folgenden informieren wir über die Verarbeitung personenbezogener Daten bei Nutzung unserer Website. Verantwortliche gem. Art. 4 Abs. 7 EU-Datenschutz-Grundverordnung (DSGVO) ist:

Lüdtke und Wadden - LaceWing Tech GbR\
c/o Lüdtke\
Waldemarstr. 54\
10997 Berlin

E-Mail: info @ lacewing.tech\
Tel.: +49 30 235 931 932

Personenbezogene Daten sind alle Daten, die auf Sie persönlich beziehbar sind, z. B. Name, Adresse, E-Mail-Adressen, Nutzer*innenverhalten.

### Erhebung personenbezogener Daten bei Besuch unserer Website

Bei der bloß informatorischen Nutzung der Website erheben wir nur die personenbezogenen Daten, die Ihr Browser an unseren Server übermittelt. Wenn Sie unsere Website betrachten möchten, erheben wir die folgenden Daten, die für uns technisch erforderlich sind, um Ihnen unsere Website anzuzeigen und die Stabilität und Sicherheit zu gewährleisten (Rechtsgrundlage ist Art. 6 Abs. 1 S. 1 lit. f DSGVO):

* IP-Adresse
* Datum und Uhrzeit der Anfrage
* Zeitzonendifferenz zur Greenwich Mean Time (GMT)
* Inhalt der Anforderung (konkrete Seite)
* Zugriffsstatus/HTTP-Statuscode
* jeweils übertragene Datenmenge
* Website, von der die Anforderung kommt
* Browser
* Betriebssystem und dessen Oberfläche
* Sprache und Version der Browsersoftware

Die oben genannten Daten (sog. Log-Dateien) werden aus Sicherheitsgründen und zur Ermittlung von Störungen für max. 7 Tage gespeichert. Anschließend werden die Log-Dateien gelöscht. Ist die Aufbewahrung von dieser Information jedoch zu Beweiszwecken (z.B. im Rahmen der Aufklärung von Missbrauchs- oder Betrugshandlungen) erforderlich, werden sie bis zur abschließenden Klärung des jeweiligen Vorfalls nicht gelöscht.

Falls wir für einzelne Funktionen unseres Angebots auf beauftragte Dienstleister zurückgreifen oder Ihre Daten für werbliche Zwecke nutzen möchten, werden wir Sie unten stehend im Detail über die jeweiligen Vorgänge informieren. Dabei nennen wir auch die festgelegten Kriterien der Speicherdauer.

### Ihre Rechte

Sie haben gegenüber uns folgende Rechte hinsichtlich der Sie betreffenden personenbezogenen Daten:

* Recht auf Auskunft
* Recht auf Berichtigung oder Löschung
* Recht auf Einschränkung der Verarbeitung
* Recht auf Widerspruch gegen die Verarbeitung
* Recht auf Datenübertragbarkeit

Sie haben zudem das Recht, sich bei einer Datenschutz-Aufsichtsbehörde über die Verarbeitung Ihrer personenbezogenen Daten durch uns zu beschweren.

### Zusammenarbeit mit Auftragsverarbeiter*innenn und Dritten

Ihre personenbezogenen Daten werden nur auf Grundlage einer gesetzlichen Erlaubnis (z.B. zur Vertragserfüllung, bei Vorliegen Ihrer Einwilligung, auf Grundlage unserer berechtigten Interessen oder bei rechtlicher Verpflichtung) an Dritter offenbart, übermittelt oder sonst Zugriff gewährt.

Werden Dritte mit der Verarbeitung von Daten auf Grundlage eines Auftragsverarbeitungsvertrags beauftragt, dürfen sie diese personenbezogenen Daten gemäß Art. 28 DSGVO nur nach unserer Weisung bearbeiten.

### Analyse-Tool Matomo (ehemals Piwik)

**Umfang und Beschreibung der Verarbeitung personenbezogener Daten**

Diese Website benutzt den Open Source Webanalysedienst Matomo der InnoCraft Ltd., der seinen Sitz in 150 Willis St, 6011 Wellington, New Zealand hat.

Matomo verwendet so genannte „Cookies“. Das sind Textdateien, die auf Ihrem Computer gespeichert werden und die eine Analyse der Benutzung der Website durch sie ermöglichen. Dazu werden die durch den Cookie erzeugten Informationen über die Benutzung dieser Website auf unserem Server gespeichert. Die so erhobenen Informationen werden ausschließlich auf unserem Server gespeichert, und zwar  folgende Daten:

* zwei Bytes der IP-Adresse des aufrufenden Systems der/des Nutzerin/Nutzers
* die aufgerufene Webseite
* die Website, von der der Nutzer auf die aufgerufene Webseite gelangt ist (Referrer)
* die Unterseiten, die von der aufgerufenen Webseite aus aufgerufen werden
* die Verweildauer auf der Webseite
* die Häufigkeit des Aufrufs der Webseite

Die Einstellungen sind so konfiguriert, dass keine persönliche Daten erhoben und gespeichert werden.

Die IP-Adresse wird vor der Speicherung anonymisiert. Das bedeutet, dass eine direkte Personenbeziehbarkeit damit ausgeschlossen wird. Die Software ist so eingestellt, dass die IP-Adressen nicht vollständig gespeichert werden, sondern 2 Bytes der IP-Adresse maskiert werden (z.B. 182.148.xxx.xxx). Auf diese Weise ist eine Zuordnung der gekürzten IP-Adresse zum aufrufenden Rechner nicht mehr möglich. Die mittels Matomo von Ihrem Browser übermittelte IP-Adresse wird nicht mit anderen von uns erhobenen Daten zusammengeführt.

**Rechtsgrundlage für die Verarbeitung personenbezogener Daten**

Die Speicherung von Matomo-Cookies und die Nutzung dieses Analyse-Tools erfolgen auf Grundlage von Art. 6 Abs. 1 lit. f DSGVO.

**Zwecke der Verarbeitung**

Die Websitebetreiber\*innen haben ein berechtigtes Interesse an der anonymisierten Analyse des Nutzerverhaltens, um sowohl sein Webangebot als auch seine Werbung zu optimieren. Durch die statistische Auswertung des Nutzer\*innenverhaltens verbessern wir unser Angebot und gestalten es für Besucher*innen interessanter.

Sofern eine entsprechende Einwilligung abgefragt wurde (z. B. eine Einwilligung zur Speicherung von Cookies), erfolgt die Verarbeitung ausschließlich auf Grundlage von Art. 6 Abs. 1 lit. a DSGVO; die Einwilligung ist jederzeit widerrufbar.

**Dauer der Speicherung**

Die Daten der hier beschriebenen Verarbeitung werden nach einer Speicherdauer von 180 Tagen automatisch gelöscht.

**Widerspruchs- und Beseitigungsmöglichkeit**

Die durch den Cookie erzeugten Informationen über die Benutzung dieser Website werden nicht an Dritte weitergegeben. Sie können die Speicherung der Cookies durch eine entsprechende Einstellung Ihrer Browser-Software verhindern; wir weisen Sie jedoch darauf hin, dass Sie in diesem Fall gegebenenfalls nicht sämtliche Funktionen dieser Website vollumfänglich werden nutzen können.

Wenn Sie mit der Speicherung und Nutzung Ihrer Daten nicht einverstanden sind, können Sie die Speicherung und Nutzung hier deaktivieren. In diesem Fall wird in Ihrem Browser ein Opt-Out-Cookie hinterlegt, der verhindert, dass Matomo Nutzungsdaten speichert. Wenn Sie Ihre Cookies löschen, hat dies zur Folge, dass auch das Matomo Opt-Out-Cookie gelöscht wird. Das Opt-Out muss bei einem erneuten Besuch dieser Website wieder aktiviert werden.

<iframe style="border: 0; height: 200px; width: 600px;" src="https://analytics.lacewing.tech/index.php?module=CoreAdminHome&action=optOut&language=de&backgroundColor=&fontColor=&fontSize=&fontFamily=sans-serif"></iframe>

Weitere Informationen zum Datenschutz finden Sie in der Datenschutzerklärung unter: [matomo.org/privacy-policy/](https://matomo.org/privacy-policy/).

### Drittstaatentransfer

Es wird sichergestellt, dass vor der Weitergabe der personenbezogenen Daten entweder ein angemessenes Datenschutzniveau besteht oder die Vereinbarung sogenannter EU Standardvertragsklauseln der Europäischen Union mit den Empfängern vorliegt.

### Löschung von Daten

Ihre Daten werden nur solange gespeichert, wie dies zur Erbringung unserer Leistungen und des Online-Angebots erforderlich ist und keine gesetzliche Aufbewahrungsfrist entgegensteht. Daten, die einer gesetzlichen Aufbewahrungsfrist unterliegen, werden bis zum Ablauf der entsprechenden Aufbewahrungsfrist gesperrt. Diese Daten stehen zur weiteren Verwendung nicht mehr zur Verfügung.

### Newsletter

Wenn Sie den auf der Website angebotenen Newsletter beziehen möchten, benötigen wir von Ihnen eine E-Mail-Adresse sowie Informationen, welche uns die Überprüfung gestatten, dass Sie der/die Inhaber*in  der angegebenen E-Mail-Adresse und mit dem Empfang des Newsletters einverstanden sind.

Zur Gewährleistung einer einverständlichen Newsletter-Versendung nutzen wir das sogenannte Double-Opt-in-Verfahren. Im Zuge dessen lässt sich der/die potentielle Empfängerin in einen Verteiler aufnehmen.  Anschließend erhält der/die Nutzerin durch eine Bestätigungs-E-Mail die Möglichkeit, die Anmeldung rechtssicher zu bestätigen. Nur wenn die Bestätigung erfolgt, wird die Adresse aktiv in den Verteiler  aufgenommen.

Diese Daten verwenden wir ausschließlich für den Versand der angeforderten Informationen und Angebote.

Als Newsletter Software wird Newsletter2Go verwendet. Seit April 2020 ist die Newsletter2Go GmbH ein Tochterunternehmen der Marke SendinBlue. Ihre Daten werden dabei an die Sendinblue GmbH (ehemals Newsletter2Go GmbH)  übermittelt. Verantwortliche Stelle im Sinne des Datenschutzrechtes ist die Sendinblue GmbH, Köpenicker Str. 126, 10179 Berlin.

Der Sendinblue GmbH ist es dabei untersagt, Ihre Daten zu verkaufen und für andere Zwecke, als für den Versand von Newslettern zu nutzen. Die Sendinblue GmbH ist ein zertifizierter Anbieter, welcher nach den Anforderungen der Datenschutz-Grundverordnung und des Bundesdatenschutzgesetzes ausgewählt wurde.

Weitere Informationen finden Sie hier: [https://de.sendinblue.com/newsletter2go/ ](https://de.sendinblue.com/newsletter2go/)

Die erteilte Einwilligung zur Speicherung der Daten, der E-Mail-Adresse sowie deren Nutzung zum Versand des Newsletters können Sie jederzeit widerrufen, etwa über den „Abmelden“-Link im Newsletter. Weitere Informationen finden Sie hier: <https://de.sendinblue.com/informationen-newsletter-empfaenger/?rtype=n2go>

Die datenschutzrechtlichen Maßnahmen unterliegen stets technischen Erneuerungen aus diesem Grund bitten wir Sie, sich über unsere Datenschutzmaßnahmen in regelmäßigen Abständen durch Einsichtnahme in  unsere Datenschutzerklärung zu informieren.

### Kontaktformular und sonstige Kontaktanfragen

Sofern Sie per Kontaktformular oder E-Mail mit uns in Kontakt treten, werden die dabei von Ihnen angegebenen Daten zur Bearbeitung Ihrer Anfrage genutzt. Die Angabe der Daten ist zur Bearbeitung und  Beantwortung Ihre Anfrage erforderlich – ohne deren Bereitstellung können wir Ihre Anfrage nicht oder allenfalls eingeschränkt beantworten.

Rechtsgrundlage für diese Verarbeitung ist Artikel 6 Absatz 1 Satz 1 Buchstabe b DSGVO, da die Verarbeitung der entsprechenden Daten zur Durchführung (vor-)vertraglicher Maßnahmen auf Ihre Anfrage hin erforderlich ist.

Ihre Daten werden gelöscht, sofern Ihre Anfrage abschließend beantwortet worden ist und der Löschung keine gesetzlichen Aufbewahrungspflichten entgegenstehen, wie bspw. solche nach Handels- oder Steuerrecht.

Das von dieser Website verwendete Kontaktformular ist von der Firma Basin. [Alles rund um das Thema Datenschutz stellt Basin hier bereit.](https://usebasin.com/gdpr)

### Online-Terminvereinbarung

Wir nutzen "Harmonizely", um eine Online-Terminvereinbarung zu ermöglichen. Harmonizely ist eine europäische Firma und bietet eine externe Plattform an, um Termine zu vereinbaren. Die Terminvereinbarung ist durch ein Skript auf unsere Website eingebettet. Wenn du den Dienst nutzt, um einen Termin zu vereinbaren, nimmst du die Dienste von Harmonizely in Anspruch. Die Datenschutzerklärung von Harmonizely findest du {{< target-blank "hier" "https://harmonizely.com/privacy/" >}}.