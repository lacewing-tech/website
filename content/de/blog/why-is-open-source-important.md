---
headline: Warum ist Open Source wichtig für die Gesellschaft?
url: blog/warum-open-source-wichtig-ist
title: Warum ist Open Source so wichtig für die Gesellschaft?
description: Was ist Open Source? Wer profitiert von Open Source und warum
  sollten wir es nutzen? Die Antworten bekommst du hier.
author: Fani Dimou
date: 2020-08-14T08:52:37.101Z
---
Open Source Software folgt einem einfachen Grundsatz: Der Code ist öffentlich zugänglich und frei verfügbar, so dass er von allen verwendet oder verändert werden kann.

Wenn du deine ersten Schritte in die Open-Source-Welt machst, denkst du vielleicht, dass du noch nie Open-Source-Software verwendet hast. Die Wahrheit ist, dass Open Source überall um uns herum ist, und manchmal benutzt man sie, ohne es überhaupt zu merken. Benutzt du zum Beispiel Firefox oder Chrome als Browser? Wenn ja, verwendest du bereits Software aus bekannten Open Source Projekten.

Lesezeit: ca. 4,5 Minuten

Foto: Richard Balog // [Unsplash](https://unsplash.com/photos/H9GZgI6jU7Y)

![Die Wichtigkeit von Open Source](/images/uploads/why-open-source-is-important-richard-balog-unsplash.jpg "Warum ist Open Source wichtig?")

## Wer profitiert von Open Source?

Open Source bietet unterschiedliche Vorteile, je nachdem, aus welcher Perspektive man es betrachtet. Einzelne Nutzer*innen profitieren direkt davon während sie es nutzen. Aber auch Start-Ups profitieren davon, denn durch den Einsatz von sicherer Open Source Software lässt sich viel Geld sparen. Im Allgemeinen hat der Einsatz von Open Source auch einen positiven Einfluss auf die Qualität und die Entwicklung der Technologie selbst. Wenn man das große Ganze betrachtet, profitiert auch die Gesellschaft in besonderer Weise davon - und das ist vielleicht der wichtigste Vorteil, den Open Source bietet.

### Warum solltest du Open Source Software nutzen?

Wenn du dich fragst, wie du als Einzelperson von dem Einsatz von Open Source Software anstelle einer Closed Source Alternative profitieren kannst, bist du schon auf dem richtigen Weg. Jede Person beginnt mit diesem Gedanken, wenn sie/er sich das erste Mal mit Open Source beschäftigt.

#### Gratis und doch voller Power

Closed Source Software wird oft für überlegen gegenüber Open Source Alternativen gehalten, wegen des Geldes, das du investierst, um sie zu nutzen. Dies ist ein Mythos. Nur weil du nicht dafür bezahlst, bedeutet es nicht, dass Open Source Software weniger Funktionen hat. Im Gegenteil - viele Menschen tragen zu Open Source Projekten bei, weil sie ihre Software nutzen wollen. Deswegen tun sie ihr Bestmögliches, um sie funktional und mit zahlreichen Features auszustatten.

#### Nach deinem Geschmack anpassen

Ein Unternehmen, das ein Closed Source Produkt herstellt, investiert eine Menge Geld, um seine Kund\*innen zufrieden zu stellen. Das bedeutet, dass die Software entwickelt wird, um die Bedürfnisse ihrer Kund\*innen und nur ihre Bedürfnisse zu befriedigen. In den meisten Fällen handelt es sich bei diesen Kund*innen um Unternehmen, die große Enterprise Edition Installationen kaufen. Wenn du also deine persönliche Closed Source Software Installation verwendest, gibt es vielleicht Funktionen, die dir nicht gefallen - aber es gibt nicht viel, was du dagegen tun kannst. Wenn dir in der Open Source Welt etwas nicht gefällt, kannst du es so ändern, wie du es haben möchtest.

#### "Muss ich dafür wissen, wie man programmiert?"

Nicht unbedingt! Wenn du dich mit Programmieren beschäftigst, dann kannst du selbst ändern, was dir nicht gefällt. Das ist jedoch nicht die einzige Möglichkeit. Denke immer daran, dass fast jedes Open Source Projekt eine Gemeinschaft hat, in der Menschen Code dazu beitragen und bereit dazu sind, anderen zu helfen. Und das ist nicht nur auf Code an sich beschränkt. Du bist mehr als willkommen, in dieser Gemeinschaft einen Fehler zu melden oder eine neue Funktion vorzuschlagen, und es ist sehr wahrscheinlich, dass andere dir bei der Umsetzung helfen werden.

### Wie kann Open Source die Technologie verbessern?

#### Allgemeine Weiterentwicklung der Technologie

Wenn eine Gruppe von Menschen täglich eine Open Source Software benutzt und sie bis an ihre Grenzen testet, entdecken sie Fehler und kommen mit neuen Ideen für neue Funktionen. Sie werden Code zur Fehlerbeseitigung beisteuern und die Gemeinschaft wird ihn übernehmen und in die kommende Version integrieren.

#### "Bedeutet das, dass die Software selbst die einzige ist, die gewinnt?"

Nein! Die gesamte Technologie entwickelt sich weiter, denn wenn Menschen aus mehreren Gemeinschaften zusammenarbeiten und Code verbreiten, werden große Erfolge erzielt, und sie sind nicht auf den Code einer Software beschränkt. Neue Ideen werden öffentlich entstehen, wiederverwendet und von anderen in Projekte integriert. Es ist kein Beitrag zu einer einzelnen Software oder einem Unternehmen, sondern ein Beitrag zur Technologie als Ganzes. 

Ein großartiges Beispiel dafür ist die Sicherheit. Wenn es eine Sicherheitslücke im Code eines Projekts gibt und ein Team sie findet, könnte ein anderes Projekt dieselbe Lücke in seinem Projekt entdecken und den gleichen Code verwenden, um sie zu schließen. Das bedeutet, dass die Sicherheit allgemein erhöht wird, was eine gute Sache ist.

#### Code ist unabhängig von Unternehmen

Ein weiterer Vorteil von Open Source ist die Abkopplung der Software von einem einzigen Unternehmen. Wenn eine Software explizit von einer Firma unterstützt wird und die Firma aus dem Geschäft aussteigt, dann gibt es auch die Software nicht mehr. Das ist bei Open Source nicht der Fall. Wenn eine Firma, die eine Open Source Software herstellt, den Betrieb einstellt, kann der Code von anderen weiterverwendet werden. Wenn die Lizenz es erlaubt, kann eine andere Firma oder Gemeinschaft das Projekt übernehmen und weiter unterstützen. Open Source Software ist nicht an ein Unternehmen gebunden; vielmehr handelt es sich um freie Technologie, die von allen genutzt und erweitert werden kann.

### Wie die Gesellschaft von Open Source profitiert

Zusätzlich zu den praktischen Vorteilen, die Open Source jedem Einzelnen und der Technologie bietet, ergeben sich aus ihrer Philosophie auch einige sozial-politische Vorteile.

#### Chancengleichheit

Es ist eine Tatsache, dass nicht alle in allen Lebensbereichen, einschließlich Technologie und Information, die gleichen Privilegien haben. Open Source macht einen großen Schritt in Richtung einer Revolution, die wir brauchen, um soziale Gerechtigkeit zu erreichen, indem es den Menschen einen freien Zugang zu Technologie ermöglicht. Sie verringert die Kluft zwischen den sozialen Ungleichheiten, was die Technologie betrifft.

Wenn Software oder Hardware (ja, es gibt auch Open Source Hardware) gleichberechtigt an die Menschen verteilt wird, dann sind wir einen Schritt näher an der Chancengleichheit beim Lernen und beim Umgang mit technischen Tools und damit an der Chancengleichheit bei z.B. Bildung oder Arbeit.

#### Öffentliche Systeme sollten öffentlichen Code verwenden

Eine weitere Möglichkeit, wie die Gesellschaft von Open Source profitieren könnte, sind Transparenz und die faire Verteilung öffentlicher Güter. Dies könnte durch den Einsatz von Open Source Software in allen öffentlichen Systemen erreicht werden. Eine demokratische Gesellschaft sollte in jedem öffentlichen System öffentlichen Code einsetzen, damit die Menschen wissen, wie ihre Daten verwendet/verarbeitet werden.

Die Verwendung von Open Source Software könnte auch zur Verbesserung eines öffentlichen Systems führen. Zum Beispiel könnte das öffentliche Gesundheitssystem durch den Einsatz von Open Source Software verbessert werden, so dass Arbeitende im Gesundheitswesen und Behörden ihre Erfahrungen miteinander teilen und das Wissen an alle verteilt wird, und nicht nur an Institutionen, die über mehr Ressourcen als andere verfügen.

### Zusammenfassung

Egal aus welcher Perspektive man es betrachtet, Open Source ist wichtig. Es hilft den Menschen, verbessert die Technologie und nicht zuletzt bietet es einige positive soziale Auswirkungen. Alles, was es braucht, ist, dass mehr Menschen Open Source nutzen, denn sie sind das Herzblut von Open Source.

- - -

**Leseempfehlung (englisch): ["Roadwork ahead - Evaluating the needs of FOSS communities working on digital infrastructure in the public interest"](https://recommendations.implicit-development.org/)**

In dem Bericht geht es um die Einzigartigkeit der Gemeinschaft, die Open Source Software entwickelt und pflegt und die Rolle, die diese Gemeinschaft bei der Gestaltung eines offenen Internets spielt. Er stützt sich auf Interviews mit Mitwirkenden an FOSS-Projekten, um sowohl die Stärken als auch die Herausforderungen der Gemeinschaft hervorzuheben. (FOSS = free and open-source software)