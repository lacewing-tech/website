---
title : "SUPERRR - BUILDING DIVERSE & EQUAL FUTURES IN TECH AND BEYOND"
description: ""
logo: "/images/uploads/superrr.svg"
externalLink: "https://superrr.net/"
draft: false
weight: 2
---

&bdquo;Wir von SUPERRR entwickeln Visionen und Projekte zu gerechten Zukünften aus einer feministischen Perspektive. Deshalb war klar: Wir suchen einen verlässlichen, fairen und kompetenten Hosting-Service mit Herz für Open-Source Technologien - und genau das haben wir mit LaceWing Tech gefunden!&rdquo;
