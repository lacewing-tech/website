---
title: "Freiberufliche:r Copy- und Creative-Content-Writer:in"
headline: "Freiberufliche:r Copy- und Creative-Content-Writer:in"
linkTitle: ""
subtitle: ""
description: ""
images:
draft: true
menu:
weight: 0
url: freiberufliche-r-copy-und-creative-content-writer
---
LaceWing Tech ist ein IT-Unternehmen mit Sitz in Berlin Kreuzberg, das sichere und benutzerfreundliche Cloud- und IT-Dienstleistungen anbietet.

## Wir bieten KMUs, Aktivist:innen und Selbstständigen

- sichere Cloud-Speicherung (Nextcloud) mit Backups,
- kollaborative Office-Tools,
- Support und Workshops zum Arbeiten mit der Cloud,
- Hardware und Consulting.

## Wir legen Wert auf

- Nachhaltigkeit,
- Bodenständigkeit und Ehrlichkeit,
- Respekt und Fairness gegenüber unseren Kund:innen und Mitarbeiter:innen,
- Inklusion,
- und ein flexibles, zukunftssicheres Unternehmen.

Wir suchen zum nächstmöglichen Zeitpunkt:

**eine:n freiberufliche:n Copy- und Creative Content-Writer:in (Englisch und Deutsch)**

Du erstellst in enger Absprache mit dem Marketing- und Sales-Team englische und deutsche Texte für unsere Website, Werbekampagnen und Informationsmaterialien. Dabei versetzt du dich in die Perspektive unserer Zielgruppen und schreibst conversion-orientierte Texte, die diese ansprechen. Für unsere Kund:innen erstellst du einfach zu verstehende Tutorials zu unseren Produkten.

Wir erwarten:
- Starke Englisch- und Deutschkenntnisse in Wort und Schrift.
- Relevante Erfahrung im Schreiben von Web- und Werbetexten. Wir erwarten von dir keine X Jahre Vollzeit-Berufserfahrung, aber die Fähigkeit, die Perspektive unserer Zielgruppen einzunehmen und conversion-orientierte Texte zu schreiben, und diese Fähigkeit mit Arbeitsproben zu veranschaulichen.
- Schnelle Auffassungsgabe und Fähigkeit, komplexe Sachverhalte zu beurteilen und zu recherchieren.
- Du interessierst dich für digitale Technologien, Datenschutz und Open Source. Du bringst idealerweise technisches Hintergrundwissen zu Cloud-Technologien mit, zumindest aber die Fähigkeit, dich schnell in neue Themen einzuarbeiten.
- Kenntnisse in SEO sind wünschenswert.

Neben der fachlichen Qualifikation erwarten wir sichere Kenntnisse in Microsoft-Office- Anwendungen. Weiterhin wünschen wir uns von Dir ein sehr gutes Kommunikationsvermögen, eine zuverlässige und pünktliche Arbeitsweise sowie die Befähigung zur Teamarbeit.

Deine Bewerbung mit einem Lebenslauf und relevanten Referenzen und Arbeitsproben schick uns bitte an folgende Mailadresse: jobs (at) lacewing.tech. Achte bitte darauf, dass die PDF-Datei nicht größer als 5 MB ist.

Fragen zur Stelle beantwortet Dir Tina unter tina (at) lacewing.tech.

Wir begrüßen insbesondere Bewerbungen von People of Color, älteren Menschen, Trans*- und nicht-binären Menschen, und Gruppen, die auf dem Arbeitsmarkt besonders benachteiligt sind. Wenn du uns diesbezüglich Informationen geben willst, die wir bei der Bewerbung berücksichtigen sollen, teile uns dies bitte bei deiner Bewerbung mit.

Bitte beachte unsere Hinweise zum Umgang mit Deinen Daten.
Informationen für Bewerber:innen im Bewerbungsverfahren:

- Wir verwenden die von Dir erhobenen Daten zum Zweck des Besetzungsverfahrens ausgeschriebener freier Arbeitsplätze.
- Rechtsgrundlage für die Datenverarbeitung ist § 26 BDSG.
- Die Bereitstellung der Daten ist für die Durchführung des Bewerbungsverfahrens notwendig. Bei Nichtbereitstellung kann die Bewerbung nicht bearbeitet werden.
- Sämtliche im Rahmen des Bewerbungsverfahrens erhobenen personenbezogenen Daten werden sechs Monate nach Beendigung des Bewerbungsverfahrens (d.h. nach Bekanntgabe der Absageentscheidung) gelöscht, es sei denn, wir sind rechtlich zur weiteren Verarbeitung Deiner Daten berechtigt oder verpflichtet.
