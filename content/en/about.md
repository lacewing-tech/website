---
draft: false
headline: About us
subtitle: ""
url: about
title: About Us
date: 2020-06-17T11:47:44.363Z
images:
weight: 3
linkTitle: About
description: Visit us to find out what LaceWing Tech is all about.
menu: main
---
{{% emphasized %}}We empower teams to work collaboratively and effectively by securing their data and giving them a holistic and sustainable IT solution - from cloud to hardware.{{% /emphasized %}}

## Our story
In light of data breaches and security risks in widely used software, businesses, organizations and individuals alike ask themselves the same question: How can we communicate and collaborate safely? Where is our data secure? The answer is LaceWing Tech.

Big tech companies like Google and Amazon offer cheap products, but lack individual service and compromise your privacy, while many open-source-alternatives, on the other hand, lack in usability. This is where LaceWing Tech comes in — we offer customers a solution that **combines security and privacy with service and usability**.

## Our products
**LaceWing Tech Ecosystem** – a holistic cloud solution in which all products and services work seamlessly together.

LaceWing Tech Ecosystem includes:
- LaceWing Cloud =  cloud hosting,
- LaceWing Hardware = pre-configured Hardware, and
- LaceWing Consult = individual support and consulting.

[Learn more about our products](/products/)

## What we stand for
### Security
We believe in securing your data. This is why we secure it with encryption. With us, you can trust that even the most sensitive business data is safe, and that your data storage is fully GDPR-compliant.  

### Usability
Efficiency and security work better when team members enjoy working with their digital tools. We make sure your technology works for you - so you can fully concentrate on your work.

### Sustainability
We take on challenges with a holistic approach. Instead of putting a bandage on problems, we work with you to find a sustainable solution that meets not only your current, but your long-term needs.

### A one-stop solution
With us, you can count on a reliable contact person for all of your IT needs.  Our products are carefully matched to make sure they work smoothly together.

## The Team
{{< figure src="/images/uploads/lwt-founders-wadden-luedtke.png" title="Copyright: Mia Steinhagen" alt="Photo of the founders Laura P. Wadden and Carma M. Lüdtke">}}

The idea for LaceWing Tech was born in 2014 when founders Carma M. Lüdtke (right) and Laura P. Wadden (left) met and recognized that they share the same vision. After that, they collaborated on various IT security projects and saw a need for secure and usable alternative to Google and Co. They founded LaceWing Tech in 2019 to offer exactly this alternative.

Today, Carma M. Lüdtke is developing the company as CEO, whereas Laura P. Wadden is ensuring the technical quality of LaceWing products as CTO. Between them, they have more than 25 years of experience in IT security, IT administration, Nextcloud and IT consulting. With LaceWing Tech, they leverage their knowledge to pursue a social purpose. Today, the LaceWing Tech team counts 11 employees and is growing steadily. We also work with a network of partners.

{{% button href="/contact/" %}}Let's talk!{{% /button %}}
