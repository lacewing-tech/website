---
title: "4 tips to get started with IT security"
headline: "4 tips to get started with IT security"
subtitle: ""
description: "Is your company just starting out with IT security? We show you the most important areas of a holistic security concept."
date: 2020-11-18T14:41:06+01:00
publishDate: 2020-11-18T14:41:06+01:00
author: "LaceWing Tech Team"
images:
- /images/uploads/get-started-with-it-security.jpg
draft: false
---

Does one of these situations sound familiar to you? You find a sensitive internal document in a team folder that everyone has access to and you ask yourself: Is this really okay? Or your team saves your passwords in a Word document and you start thinking: Is that really secure?
 
In this article, we show you how to start with IT security in your company if you don't yet have a security concept.

{{< figure src="/images/uploads/get-started-with-it-security.jpg" title="Image: Patrick Perkins // Unsplash" alt="" width="780">}}

## How do I start with IT security?
Before you start thinking about concrete security measures, the first step should always be to assess the current situation. This is the only way you can determine the areas you should improve. The following questions can help you to assess your current status:
 
- What data does your company process?
- Which of this data is sensitive? 
- How are your files currently shared and stored, and by whom?
 
It is important to understand that IT security can only work if your security concept is holistic. For example, if you only improve your technical infrastructure but don’t make sure that everyone is comfortable using it, your concept won't work.
 
For a holistic concept you should think about the following four points: 
### 1. Educate your employees on security issues
The training of your employees and an open culture of constructive feedback are important components of a holistic IT security concept. When it comes to IT security, mistakes are unavoidable, but it is crucial that you deal with these mistakes openly. All team members should be able to express their uncertainties and mistakes without fear. They should be able to discuss and address them openly, without having to expect negative consequences.
 
Security is teamwork. It is the task of the entire team to create structures that enable each individual to follow and understand the security guidelines. An important measure to achieve this is regular training. In these training sessions, you can answer questions such as how to create secure passwords or how to safely use public wifi. 
 
In addition to internal trainings, you can also take advantage of various other measures to make sure your team is up-to-date regarding IT security topics:

- An introduction to IT security as an integral part of your onboarding for new employees
- A newsletter with regular safety tips
- An IT security guideline to summarize the most important rules for all to refer to if needed
- External workshops on selected topics as additional training (by the way: LaceWing Tech offers workshops that are tailored to the needs of your team. [Contact us for a consultation.](/contact/))

### 2: Decide to use secure, user-friendly software by default
Imagine an employee is under extreme time pressure and has to meet an important deadline. They try to upload the data to your cloud to share with an external partner, but it doesn't work. However, dragging the sensitive data onto a USB stick and then share it is done in a few minutes and the deadline can be met. What would you choose in this situation?
 
A secure cloud is only half the story. It has to be user-friendly, too. If you have a secure cloud but it is unreliable, or very complicated to use, your employees will switch to simpler alternatives depending on the situation. 

Security should be built into the software you use as a standard feature, not just an extra option as and when you need it. For example, if your cloud is encrypted by default, your data is safe; you don't have to worry about additional security settings.
 
One way you can set up a secure and yet user-friendly cloud is to use Nextcloud through our [Lacewing Cloud](/products/). As you are used to with other setups, you can store, synchronize or share data. The difference: your data is securely encrypted using Server Side Encryption. 

### 3. Check who has access to your offices
Have you ever thought about how secure your office actually is? Maybe your offices are empty and unattended during lunch breaks. Would it be conceivable that an unauthorized person comes into the building during this time and looks at sensitive documents on the desks, or even gains access to an unsecured computer?

The security of your offices is an important component of your security concept. Think about the following questions:
 
- Do you lock your rooms when all employees are out of the office, e.g on their lunch break?
- Who has access to the offices? 
- Are sensitive files locked in a filing cabinet?
- Is sensitive data often left open on the desk? 
 
A widespread misconception is that you need a server directly in your own office (on-premise cloud) to ensure high data security. However, this is often not the case because your office can be broken into. In most cases, your server room is much less secure than a hosted cloud. [We would be happy to advise you on which solution (on-premise, hosted or hybrid) is the best for you.](/contact/)

### 4. Make regular backups
Sometimes, IT security has nothing to do with attacks by external parties. You have probably at some point deleted a document by mistake, or been unable to access your data due to some technical defect. Things like these happen often.
 
The loss of data in companies is a common problem that leads to a large financial loss every year. In a survey conducted by Dell Technologies, 82 percent of the companies surveyed said they had problems with downtime or data loss last year and they expected these problems to increase in the future.
 
This makes it all the more important to make sure you perform your own backups, securely and regularly. You could even set up an automated process so that you won’t forget to make backups.

For extra peace of mind, data stored in your LaceWing Cloud is backed up regularly, and automatically. You can even easily browse and restore previous versions of a document!

We hope that these tips have given you a good introduction to data security. If you want to learn more about IT security for small and medium-sized businesses, [let us know!](/contact/)
