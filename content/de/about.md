---
draft: false
headline: Über uns
subtitle: ""
url: ueber-uns
title: LaceWing Tech | Über uns
date: 2020-06-19T09:45:51.050Z
images:
weight: 3
linkTitle: Über uns
description: "Erfahrt hier mehr über uns: Wer steht hinter LaceWing Tech, und wofür stehen wir?"
menu: main
---
{{% emphasized %}}Wir ermöglichen es Teams, kollaborativ und effektiv zu arbeiten, indem wir ihre Daten sichern und ihnen von Cloud bis Hardware eine ganzheitliche, nachhaltige IT-Lösung geben.{{% /emphasized %}}

## Unsere Geschichte
Angesichts von Datenskandalen und Sicherheitsrisiken gängiger Software stellen sich kleine und mittelständische Firmen, Organisationen und Privatpersonen die gleiche Frage: Wie können wir unbedenklich kommunizieren und zusammenarbeiten? Wo sind unsere Daten sicher? Die Antwort liegt bei LaceWing Tech.

IT-Konzerne wie Google und Amazon bieten zwar kostengünstige Produkte, dafür gibt es bei ihnen keinen individuellen Service und vor allem keinen Datenschutz. Demgegenüber sind Open-Source-Alternativen oftmals nicht intuitiv bedienbar. Hier kommt LaceWing Tech ins Spiel: Wir bieten unseren Kund:innen eine Lösung, die Sicherheit mit Service und Benutzer:innenfreundlichkeit vereint.

## Unsere Produkte
**LaceWing Tech EcoSystem** – eine ganzheitliche Cloud-Lösung mit Produkten und Dienstleistungen, die aufeinander basieren.

LaceWing Tech EcoSystem beinhaltet:
- LaceWing Cloud =  Cloud-Hosting,
- LaceWing Hardware = vorkonfigurierte Hardware und
- LaceWing Consult = individuellen Support und Beratung.

[Erfahre mehr über unsere Produkte](/de/produkte/)

## Dafür stehen wir
### Sicherheit
Wir glauben an den Schutz deiner Daten. Deshalb schützen wir sie mit Verschlüsselung. Bei uns kannst du dich darauf verlassen, dass auch sensible Unternehmensdaten jederzeit sicher sind und deine Datenspeicherung die Vorgaben der DSGVO erfüllt.

### Nutzer:innenfreundlichkeit
Effizientes Arbeiten und Sicherheit funktionieren nur, wenn alle im Team gerne mit der Software arbeiten. Wir sorgen dafür, dass die Technik für dich arbeitet - damit du dich ganz auf deine Arbeit konzentrieren kannst.

### Nachhaltigkeit
Bei allen unseren Produkten setzen wir auf einen ganzheitlichen Ansatz. Statt ein Pflaster auf Probleme zu kleben, erarbeiten wir mit dir gemeinsam eine nachhaltige Lösung, die nicht nur heute, sondern langfristig zu deinen Bedürfnissen passt.

### Alles aus einer Hand
Mit uns hast du jederzeit eine*n verlässlichen Ansprechpartner:in für alle deine IT-Bedürfnisse. Alle unsere Produkte sind aufeinander abgestimmt, sodass sie reibungslos miteinander funktionieren.

## Das Team
{{< figure src="/images/uploads/lwt-founders-wadden-luedtke.png" title="Urheber:in: Mia Steinhagen" alt="Foto der Gründer:innen Laura P. Wadden and Carma M. Lüdtke">}}

Die Idee für LaceWing Tech entstand 2014, als sich die Gründer:innen Carma M. Lüdtke (rechts im Bild) und Laura P. Wadden (links im Bild) kennenlernten und feststellten, dass sie eine gemeinsame Vision teilen. Danach arbeiteten sie gemeinsam an diversen IT-Sicherheits-Projekten. Dabei wurde ihnen klar, dass es einen großen Bedarf an sicheren und nutzer:innenfreundlichen Alternativen zu Google und Co. gibt. 2019 gründeten sie schließlich LaceWing Tech, um genau diese Alternative zu bieten.

Heute entwickelt Carma M. Lüdtke als CEO das Unternehmen weiter, während Laura P. Wadden als CTO die technische Qualität der Produkte sicherstellt. Gemeinsam verfügen sie über 25 Jahre an Erfahrung in IT-Sicherheit, IT-Administration, Nextcloud und IT-Consulting. Mit den Produkten von LaceWing Tech setzen sie  ihr Wissen sinnvoll und sozial verträglich ein. Das Team von LaceWing Tech zählt mittlerweile 11 Festangestellte und wächst weiter. Darüber hinaus arbeiten wir mit einem Netzwerk an Partner:innen zusammen.  

{{% button href="/de/kontakt/" %}}Lass uns sprechen!{{% /button %}}
