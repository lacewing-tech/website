---
draft: true
title: IT Administrator / Support Engineer
headline: IT Administrator / Support Engineer
subtitle: ""
url: /jobs/it-administrator/
weight: 0
description: We are looking for two full or part-time IT administrators / support engineers.
images:
date: 2020-12-04T15:53:04.253Z
linkTitle: ""
menu: null
---
LaceWing Tech is a 10-person IT company located in Berlin, Kreuzberg. We offer open source cloud storage (Nextcloud) and IT services (support, workshops, installation and consulting) with a focus on security, usability and sustainability. Our clients are mostly small- to medium-sized companies, NGOs, and freelancers.

We care about being grounded and honest, inclusive, respectful and fair to our customers and colleagues, technically excellent, flexible and future-proof.

Learn more about us on www.lacewing.tech.

**\- Application deadline extended to December 10, 2020 -**

## We are looking for:

**two full or part-time IT Administrators / Support Engineers**

(January start)

Your main responsibility is to ensure that clients and staff have the technology they need and can use it safely and with as few headaches as possible. As part of the IT Services and Internal IT team, your role is to build and maintain our IT systems, provide support and training internally and externally to staff and clients, and produce excellent documentation. This role is vital at LaceWing Tech because we believe that technology is only secure when people are able to use it properly.

### On a daily basis, you'll:

* manage our helpdesk (internal and external) - prioritize, answer and analyze support tickets and come up with systems and processes to improve work flow
* co-design and give workshops for clients on our products and IT security (Nextcloud, Password Managers, IT Security Foundations, etc)
* setup and maintain soft- and hardware (internal and external)
* co-create & maintain internal IT systems and processes with an eye on quality, security and continuous improvement
* investigate and get to the bottom of IT issues and incidents, including documentation and how-to guides for frequent issues

### Requirements:

* Excellent German and English communication skills (written and spoken), especially with clients
* Relevant experience in IT Administration and/or Support and/or Training. We don’t need you to show X years of full-time experience, but you should be able to work (rather) independently to get to the bottom of incidents and issues, improvise, quickly research and troubleshoot multiple IT topics, and problem-solve.
* Deep(er) knowledge in either MacOS or Windows; experience with Linux, Nextcloud and Email Clients is a plus

### You:

* are organized - you prioritize among multiple projects and tasks, document & communicate progress, and meet deadlines
* are eager to share knowledge and learn through pairing, presentations and documentation
* explain incidents and issues in a clear way and you adjust the level of technical detail to your audience
* understand that there are a multitude of factors that impact peoples’ ability to use and access technology - your approach takes this context into account without being patronizing or taking a “tech saviour” approach
* are patient and forgiving of mistakes (also your own!) and see them as an opportunity to learn
* like trying new things and ideas
* care about open source and finding non-proprietary solutions

Apart from these skills, we expect you to be good at problem-solving and working in a small team. Right now we work from home due to Corona and sometimes meet in person for team meetings. Eventually, as the situation allows, we will work partly from our office and when necessary provide in-person trainings, installations and support.

Please send your application – resume and a short cover letter – to [jobs @ lacewing.tech](mailto:jobs@lacewing.tech) by December 10. The PDF file should not be larger than 5 MB.

If you have any questions, please contact Carma: [carma @ lacewing.tech](mailto:carma@lacewing.tech).

We encourage applications from BIPOC, older people, trans* and non-binary people, and members of groups who often encounter systemic barriers to employment. If there are ways in which you identify that you would like us to take into consideration, please let us know on your application.

We care about privacy. Please review how we handle your data below.

Information for applicants during the application process:

* We process your data in order to fill open job vacancies and to manage the application process.
* Legal basis for data processing is § 26 BDSG.
* This data is necessary for conducting the application process. If you don't provide this data, we cannot process your application.
* All personal data regarding the application process will be deleted six months after the application process is terminated (which means six months after you received a rejection from us), except when we are legally justified or required to process your data further.
